import Vue from 'vue';
import VueResize from 'vue-resize';
import VueElectron from 'vue-electron';
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/ru-RU';
// Шина событий. Попробовать потом избавиться от нее.
import VueEvents from 'vue-events';
import VDragged from 'v-dragged';
import Vuebar from 'vuebar';
import Vuelidate from 'vuelidate';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import 'element-ui/lib/theme-chalk/index.css';
import 'vue-resize/dist/vue-resize.css';
import ControlComponents from './_library/controlComponents';
import BlockConfigComponents from './_library/functionalBlocks/index.components.js';
import DeviceConfigComponents from './_library/devices/index.components.js';
import WrapperSettingsBlock from './_library/WrapperSettingsBlock';

// Mimic для тестирования backEnd
// import 'mimic'; // eslint-disable-line
import App from './App';
import store from './store';

import { ipcRenderer as ipc } from 'electron'; //eslint-disable-line


ipc.on('logFromMain', (event, data) => {
  console.log(data);
});
// globally (in your main .js file)
Vue.use(VueResize);
Vue.use(VueElectron);
Vue.use(ElementUI, { size: 'mini', locale });
Vue.use(VueEvents);
Vue.use(VDragged);
Vue.use(Vuebar);
Vue.use(Vuelidate);
Vue.use(ControlComponents);
Vue.use(BlockConfigComponents);
Vue.use(DeviceConfigComponents);

library.add(fas, far, fab);
Vue.component('icon', FontAwesomeIcon);
Vue.component('WrapperSettings', WrapperSettingsBlock);


if (!process.env.IS_WEB) Vue.use(require('vue-electron'));
Vue.config.productionTip = false;

/* eslint-disable no-new */
const vm = new Vue({
  el: '#app',
  render: h => h(App),
  store,
});


// Обработчик промиса, с последующим отображением окна сообщения пользователю
vm.$root.$wrapperMsg = promise => {
  if (promise instanceof Promise)
    promise
      .then(info => {
        if (info)
          vm.$message({
            showClose: true,
            message: info,
            type: 'success',
          });
      })
      .catch(err => {
        console.log(err);
        vm.$message({
          showClose: true,
          message: err.message,
          type: 'error',
        });
      });
};
// Создаем placeholder для иконки drag elementa
vm.$root.dragImage = text => {
  /* Canvas для отображения Title Drag  */
  const canvas = document.getElementById('canvasDrag');
  const ctx = canvas.getContext('2d');
  const pixelRatio = window.devicePixelRatio || 1;
  ctx.imageSmoothingEnabled = false;
  ctx.mozImageSmoothingEnabled = false;
  ctx.imageSmoothingEnabled = false;
  ctx.scale(pixelRatio, pixelRatio);
  canvas.height = 20 * pixelRatio;
  canvas.width = Math.round(ctx.measureText(text).width + 40) * pixelRatio;
  canvas.style.height = `${+canvas.height / pixelRatio}px`;
  canvas.style.width = `${+canvas.width / pixelRatio}px`;
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.font = '24px sans-serif';
  ctx.textBaseline = 'middle';
  ctx.textAlign = 'center';
  ctx.fillText(text, Math.round(canvas.width / 2) + 10, 10 * pixelRatio);
  return canvas;
  /* __________________ */
};
vm.$root.$data.os = process.platform;
vm.$root.$data.value = {
  width: {
    treeAddress: 200,
    treeDevice: 200,
    treeBlocks: 200,
    treeLeftBlock: 600,
  },
  height: {
    terminal: 200,
  },
};
