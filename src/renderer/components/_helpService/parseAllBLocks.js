// const blocks = require('../../_library/functionalBlocks');
const fs = require('fs');
const path = require('path');
const packBlock = require('./packBlock');
const pathDir = './src/renderer/_library/functionalBlocks/structures';

const pathBlocks = fs.readdirSync(pathDir);



let file = '# Порядок отправки блоков \n';

const arrayBlocks = pathBlocks.map(el => require(path.resolve(`${pathDir}/${el}`))); //eslint-disable-line

arrayBlocks
  .sort((a, b) => a.fnTypeValue - b.fnTypeValue)
  .forEach(item => {
    file += packBlock(item);
    file += '---- \n';
  });

fs.writeFile('_doc/Blocks.md', file, 'utf8', err => {
  if (err) console.log(err);
  else console.log('Markdown file created.');
});
console.log(`Всего в библиотеке ${arrayBlocks.length} блоков`);



