const pathFile = '/Users/anton/Desktop/Шарин_квартира.json';
const fs = require('fs');
const path = require('path');
let projectData = null;

let pathCount = 0;
const resolveFile = () => {
  projectData.devices.elements.forEach(array => {
    array.forEach(device => {
      if (device.paths) {
        device.paths.forEach(_path => {
          _path.position.sPoint = [];
          _path.position.tPoint = [];
          _path.position.typeLine = null;
          _path.position.supLine = {};
          _path.position.supLine.sCoord = [];
          _path.position.supLine.tCoord = [];
          pathCount += 1;
        });
      }
    });
  });
  console.log(`Обработано ${pathCount} соединения`);
};

fs.readFile(pathFile, (err, data) => {
  if (err) {
    console.log(err);
    console.log('Проблемы с открытием файла');
  } else {
    console.log('Файл успешно открыт');
    projectData = JSON.parse(data);
    console.log('Файл успешно распознан');
    resolveFile();
    console.log('Файл успешно преобразован');
    const pathNewfile = `${path.dirname(pathFile)}/${path.basename(pathFile, '.json')}_new.json`;
    console.log(pathNewfile);
    fs.writeFile(pathNewfile, JSON.stringify(projectData), err => {
      if (err) console.log('Ошибка записи файла');
      else console.log('Файл успешно записан');
    });
  }
});
