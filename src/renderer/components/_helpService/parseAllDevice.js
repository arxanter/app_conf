// const blocks = require('../../_library/functionalBlocks');
const fs = require('fs');
const path = require('path');
const packBlock = require('./packBlock');
const pathDir = './src/renderer/_library/devices/structures';

console.log('Начинаем упаковку устройств');
console.log('******************');
const pathDevices = fs.readdirSync(pathDir);
// Импортируем все устройства из библиотеки
const devices = pathDevices.map(el => require(path.resolve(`${pathDir}/${el}`))); //eslint-disable-line

const easyDevices = devices.filter(el => !el.params.isFBD);

easyDevices.forEach(device => {
  console.log(device.fnType);
  let file = `# Порядок отправки блоков устройства ${device.fnType}\n`;
  device.blocks
    .sort((a, b) => a.fnTypeValue - b.fnTypeValue)
    .forEach(item => {
      file += packBlock(item);
      file += '---- \n';
    });
  fs.writeFile(`_doc/${device.name}_blocks.md`, file, 'utf8', err => {
    if (err) console.log(err);
  });
});

console.log(`Всего в библиотеке ${easyDevices.length} простых устройств`);
