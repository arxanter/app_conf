/**
 *
 * @param {Значение параметра для записи в буфер} param
 * @param {Тип данных параметра} dataType
 */
function writeData(param, dataType) {
  let dataReturn = 0;
  // Определяем длинну параметра по его типу данных
  switch (dataType) {
    case 'uint8':
      dataReturn = 1;
      break;
    case 'uint16':
      dataReturn = 2;
      break;
    case 'float':
      dataReturn = 4;
      break;
    case 'string':
      // Условие, что длина строки 50 байт
      dataReturn = 50;
      break;
    case 'mac':
      dataReturn = param.split(':').length;
      break;
    case 'arrayBits':
      do {
        if (param.length % 8 !== 0) param.push(0);
      } while (param.length % 8 !== 0);
      for (let i = 0; i < Math.round(param.length / 8); i++) {
        dataReturn += 1;
      }
      break;
    case 'arrayUint8':
      param.forEach(() => {
        dataReturn += 1;
      });
      break;
    case 'arrayUint16':
      param.forEach(() => {
        dataReturn += 2;
      });
      break;
    default:
      throw new Error(`Unknow type value: ${dataType}`);
  }
  // Возвращаем его размер в байтах
  return dataReturn;
}
/**
 *
 * @param {Блок, который запаковывается} block
 */
module.exports = block => {
  console.log(`Тип блока: ${block.fnType}`);
  let data = `## ${block.fnType} \n`;
  data += `## LogicTypeID: ${block.fnTypeValue} \n`;
  let countByte = 0;
  data += `#### Параметры блока:\n`;
  data += `Название | Размер | Позиция в масиве\n`;
  data += `--- | --- | --- \n`;
  data += ` ID блока | 1 | ${countByte}\n`;
  countByte += 1;
  data += ` Тип блока | 1 | ${countByte}\n`;
  countByte += 1;
  data += ` HASH_UID | 4 | ${countByte}\n`;
  countByte += 4;
  data += ` clearVariables | 1 | ${countByte}\n`;
  countByte += 1;

  data += `#### Настройки:\n`;
  data += `Имя параметра | Тип значения | Размер | Позиция в масиве\n`;
  data += `--- | --- | --- | ---\n`;

  // Если блок имеет раздел настройки начинаем пробегаться по ним
  // Для определения размера, который они будут занимать
  if (block.settings) {
    Object.keys(block.settings)
      .sort()
      .forEach(name => {
        const type = block.settingsTypes[name];
        const value = block.settings[name];
        let countValue = 0;
        // Check if param is Array of value. And check if that value type - arrayType. Then write value to buffer.
        if (type === undefined) {
          // console.log(`Параметр ${name} не отправляется`);
        } else {
          //  Проверяем тип данных
          if (
            typeof value === 'number' ||
            typeof value === 'string' ||
            (type.includes('array') && typeof value[0] === 'number')
          ) {
            countValue = writeData(value, type);
          } else if (Array.isArray(value) || (type.includes('array') && Array.isArray(value[0]))) {
            value.forEach(el => {
              countValue += writeData(el, type);
            });
          }
          data += `${name} | ${type} | ${countValue} | ${countByte}\n`;
          countByte += countValue;
        }
      });
  }

  // Объект 'output' занимает 7 байт в буфере, а 'input' занимает 31 байт.
  const inputObjects = block.objects.filter(el => el.direction === 'input');
  const outputObjects = block.objects.filter(el => el.direction === 'output');
  data += `#### Объекты INPUT:\n`;
  data += `Имя | Размер | Позиция в масиве\n`;
  data += `--- | --- | --- \n`;
  inputObjects.forEach(object => {
    data += `${object.name} | 31 | ${countByte} \n`;
    countByte += 31;
  });
  data += `#### Объекты OUTPUT:\n`;
  data += `Имя | Размер | Позиция в масиве\n`;
  data += `--- | --- | --- \n`;
  outputObjects.forEach(object => {
    data += `${object.name} | 7 | ${countByte} \n`;
    countByte += 7;
  });
  data += `#### Общая длина переданных данных: ${countByte} byte\n`;
  return data;
};
