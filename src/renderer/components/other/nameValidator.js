const nameValidator = (value) => {
  const reg = /[A-zА-я\w'\-_.]{1,}/;
  return reg.test(value);
};

export default nameValidator;
