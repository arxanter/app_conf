export default {
  computed: {
    viewMode() {
      return this.$store.state.visio.mode;
    },
    isDisabledRoot() {
      return this.viewMode !== 'production';
    },
    isEditElement() {
      return this.$store.state.visio.editElement === this.data;
    },
  },
  methods: {
    dataSend(msg, status = null) {
      console.log(msg);
      console.log(`Статус: ${status}`);
      if (msg.data.address !== null && msg.data.address !== undefined) {
        if (status !== null) this.$store.commit('SET_STATUS_VALUE', { status, value: msg.data.value });
        if (this.$root.$socket) this.$root.$socket.send(msg);
      }
    },
  },
};
