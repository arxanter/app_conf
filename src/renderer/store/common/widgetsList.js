export default [
  {
    widgetType: 'wClimate',
    type: 'widget',
    settings: {
      name: 'Значение',
      step: 0.1
    },
    addresses: [
      { valueName: 'setpoint', address: null, status: null, dataType: 6 },
      { valueName: 'mode', address: null, status: null, dataType: 2 },
      { valueName: 'value', status: null, dataType: 6 },
      { valueName: 'heat', status: null, dataType: 1 },
      { valueName: 'cool', status: null, dataType: 1 },
    ],
  },
  {
    widgetType: 'wHeader',
    type: 'widget',
    settings: {
      name: 'Значение',
    },
  },
  {
    widgetType: 'wInputNumber',
    type: 'widget',
    settings: {
      name: 'Значение',
      step: 1,
      min: 0,
      max: 255,
    },
    addresses: [{ valueName: 'value', address: null, status: null, dataType: 2 }],
  },
  {
    widgetType: 'wLED',
    type: 'widget',
    settings: {
      name: 'Значение',
    },
    addresses: [
      { valueName: 'value', address: null, status: null, dataType: 7 },
      { valueName: 'toggle', address: null, status: null, dataType: 1 },
    ],
  },
  {
    widgetType: 'wScenes',
    type: 'widget',
    settings: {
      name: 'Значение',
      countScenes: 3,
      scenes: [
        {
          icon: ['far', 'lightbulb'],
          value: 1,
        },
        {
          icon: ['far', 'lightbulb'],
          value: 2,
        },
        {
          icon: ['far', 'lightbulb'],
          value: 3,
        },
        {
          icon: ['far', 'lightbulb'],
          value: 4,
        },
        {
          icon: ['far', 'lightbulb'],
          value: 5,
        },
        {
          icon: ['far', 'lightbulb'],
          value: 6,
        },
        {
          icon: ['far', 'lightbulb'],
          value: 7,
        },
        {
          icon: ['far', 'lightbulb'],
          value: 8,
        },
      ],
    },
    addresses: [{ valueName: 'value', address: null, dataType: 2 }],
  },
  {
    widgetType: 'wSlider',
    type: 'widget',
    settings: {
      name: 'Значение',
      demension: '%',
      isSwitch: true,
      min: 0,
      max: 100,
    },
    addresses: [
      { valueName: 'value', address: null, status: null, dataType: 2 },
      { valueName: 'toggle', address: null, status: null, dataType: 1 },
    ],
  },
  {
    widgetType: 'wStatusIcon',
    type: 'widget',
    settings: {
      name: 'Значение',
      countState: 2,
      states: [
        {
          icon: ['far', 'lightbulb'],
          color: '#909399',
          value: 0,
        },
        {
          icon: ['far', 'lightbulb'],
          color: '#909399',
          value: 1,
        },
        {
          icon: ['far', 'lightbulb'],
          color: '#909399',
          value: 0,
        },
        {
          icon: ['far', 'lightbulb'],
          color: '#909399',
          value: 0,
        },
        {
          icon: ['far', 'lightbulb'],
          color: '#909399',
          value: 0,
        },
      ],
    },
    addresses: [{ valueName: 'value', status: null, dataType: 1 }],
  },
  {
    widgetType: 'wStatusValue',
    type: 'widget',
    settings: {
      name: 'Значение',
      demension: '°C',
    },
    addresses: [{ valueName: 'value', status: null, dataType: 2 }],
  },
  {
    widgetType: 'wSwitch',
    type: 'widget',
    settings: {
      name: 'Значение',
      inverse: 0,
      valueOn: 0,
      valueOff: 255,
    },
    addresses: [{ valueName: 'value', address: null, status: null, dataType: 1 }],
  },
];
