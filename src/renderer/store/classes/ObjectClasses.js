import Vue from 'vue';
class Address {
  constructor(name = 'Адрес', bus = 1) {
    this.id = null;
    this.parentID = null;
    this.type = 'element';
    this.subtype = 'address';
    this.name = name;
    this.tag = null;
    this.sockets = [];
    this.dataType = null;
    this.bus = bus;
    this.dataType = null;
  }
}
/**
 *
 *
 */
class AddressBlock {
  constructor(address, top, left) {
    this.address = address;
    this.props = { top, left };
  }
}
/**
 *
 *
 */
class Connection {
  constructor({ source, target, address }) {
    this.addressID = address.id;
    this.position = {
      sPoint: [],
      tPoint: [],
      sLine: null,
      tLine: null,
      typeLine: null,
      supLine: {
        sCoord: [0, 0],
        tCoord: [0, 0],
      },
    };
    this.source = {
      type: source.type,
      blockID: source.type === 'addressSocket' ? source.address.id : source.block.id,
      socketIndex: null,
      DOM_ID: '',
    };
    this.target = {
      type: target.type,
      blockID: target.type === 'addressSocket' ? target.address.id : target.block.id,
      socketIndex: null,
      DOM_ID: '',
    };
    this.source.socketIndex =
      source.type === 'addressSocket'
        ? source.direction
        : source.block.objects.findIndex(el => el.name === source.object.name);
    this.target.socketIndex =
      target.type === 'addressSocket'
        ? target.direction
        : target.block.objects.findIndex(el => el.name === target.object.name);
    this.source.DOM_ID = `${this.source.type}-${this.source.blockID}-${this.source.socketIndex}`;
    this.target.DOM_ID = `${this.target.type}-${this.target.blockID}-${this.target.socketIndex}`;
  }
}
/**
 *
 *
 */
class TreeClass {
  constructor(name) {
    this.name = name;
    this.id = 0;
    this.isOpen = true;
    this.type = 'folder';
    this.folders = [[]];
    this.elements = [[]];
    this.elementsID = [true];
    this.foldersID = [true];
    this.tag = [];
  }
}
/**
 *
 *
 */
class TreeStruct {
  constructor(name, type, nameItem) {
    this.type = type;
    this.props = {
      nameItem,
      changeElement: null,
      dragElements: [],
      renameEl: null,
    };
    this.tree = new TreeClass(name);
  }

  get getNewTree() {
    return new TreeClass(this.tree.name);
  }

  // Получение нового ID элемента структуры
  get getElementID() {
    let id = this.tree.elementsID.length;
    for (let i = 1; i < id; i += 1) {
      if (this.tree.elementsID[i] === undefined || this.tree.elementsID[i] === null) {
        id = i;
        break;
      }
    }
    return id;
  }

  // Получения нового ID папки структуры
  get getFolderID() {
    let id = this.tree.foldersID.length;
    for (let i = 1; i < id; i += 1) {
      if (this.tree.foldersID[i] === undefined || this.tree.foldersID[i] === null) {
        id = i;
        break;
      }
    }
    return id;
  }

  get dragType() {
    let element = 0;
    let folder = 0;
    this.props.dragElementsforEach(el => {
      if (el.type === 'folder') folder += 1;
      if (el.type === 'element') element += 1;
    });
    if (element && folder) return 'mixed';
    if (element) return 'elements';
    return 'folders';
  }

  getElementByID(id) {
    return _.flatten(this.tree.elements).find(el => el.id === id);
  }
  getIndexElement(element) {
    if (element.type === 'element') return this.tree.elements[element.parentID].indexOf(element);
    if (element.type === 'folder') return this.tree.folders[element.parentID].indexOf(element);
    return -1;
  }
  getFolderByID(id) {
    if (id === 0) return this.tree;
    return _.flatten(this.tree.folders).find(el => el.id === id);
  }

  collapseFolder(folder) {
    this.tree.folders[folder.id].forEach(el => {
      el.isOpen = false;
    });
  }
  // Сеттеры статусов конец

  // Проверка имени массива
  checkNameArray(newName, nameArray) {
    let array = _.flatten(this.tree[nameArray]);
    array = _.compact(array);
    let countName = 0;
    let pureName = 0;
    let name = newName;
    // Проверка на имя без индекса
    array.forEach(el => {
      if (el.name.split('(')[0] === name) countName += 1;
      if (el.name === name) pureName += 1;
    });
    if (countName === 0 || pureName === 0) return name;
    // Проверка если есть имена с индексом
    let i = 1;
    while (i) {
      name = `${name.split('(')[0]}(${i})`;
      let count = 0;
      for (let i = 0; i < array.length; i += 1) {
        if (array[i].name === name) count += 1;
      }
      i = count ? i + 1 : 0;
    }
    return name;
  }

  // Обновление tag папок при переименовании или перетаскивании в др. папку
  updateTagFolder(folder) {
    const parent = this.getFolderByID(folder.parentID);
    folder.tag = parent.tag.concat(folder.name);
    this.tree.folders[folder.id].forEach(el => this.updateTagFolder(el));
    this.tree.elements[folder.id].forEach(el => {
      el.tag = folder.tag;
    });
  }

  // Добавление новой папки в структуру
  /**
   *
   * @param {Имя папки для проверки} name
   * @param {Аргументы} args 1 param  - parent, 2 param - hidden (true/false)
   */
  addFolder(name, ...args) {
    const folder = {
      name: null,
      id: null,
      type: 'folder',
      parentID: null,
      isOpen: false,
      tag: null,
      hidden: args[1] || false,
    };
    // Заполняем струкутуру папки.
    const parent = args[0] || this.tree;
    folder.name = this.checkNameArray(name, 'folders');
    folder.id = this.getFolderID;
    this.tree.foldersID[folder.id] = true;
    folder.parentID = parent.id;
    folder.tag = parent.tag.concat(folder.name);
    Vue.set(this.tree.folders, folder.id, []);
    Vue.set(this.tree.elements, folder.id, []);
    this.tree.folders[parent.id].unshift(folder);
    return folder;
  }

  // Добавление нового элемента
  addElement(element, parent, id = null) {
    element = _.cloneDeep(element);
    // Получаем ID нового элемента
    element.id = id || this.getElementID;
    parent = parent || this.tree;
    // Занимаем это ID
    this.tree.elementsID[element.id] = true;
    // Кладем в массив элементов родителя
    element.parentID = parent.id;
    // Проверяем имя на дубликат
    element.name = this.checkNameArray(element.name, 'elements');
    /**
     *
     */
    // Присваиваем теги
    element.tag = [].concat(parent.tag);
    // Добавляем новые элемент в структуру. Если уже есть папка, то вставляем в начало. Если нет, создаем ее.
    if (!this.tree.elements[parent.id]) Vue.set(this.tree.elements, parent.id, []);
    this.tree.elements[parent.id].unshift(element);
    return element;
  }

  deleteFolder(element) {
    const index = this.tree.folders[element.parentID].indexOf(element);
    if (index !== -1) {
      this.tree.folders[element.parentID].splice(index, 1);
      // Освобождаем ID папки
      Vue.set(this.tree.foldersID, element.id, undefined);
      // Очищаем массив элементов
      Vue.set(this.tree.elements, element.id, []);
    }
  }

  // Удаление элемента структуры
  deleteElement(element) {
    const index = this.tree.elements[element.parentID].indexOf(element);
    if (index !== -1) {
      // Удаляем сам элемент
      this.tree.elements[element.parentID].splice(index, 1);
      // Освобождаем ID
      Vue.set(this.tree.elementsID, element.id, undefined);
    }
  }

  // Редактирования имени папки
  editFolder(element, name) {
    element.name = this.checkNameArray(name, 'folders');
    element.tag.splice(-1, 1, element.name);
    this.tree.folders[element.id].forEach(el => this.updateTagFolder(el));
    this.tree.elements[element.id].forEach(el => {
      el.tag = element.tag;
    });
  }

  // Редактирования имени элемента
  editElement(element, name, address) {
    if (element.name !== name) element.name = this.checkNameArray(name, 'elements');
    if (address && address !== element.id) {
      Vue.set(this.tree.elementsID, element.id, undefined);
      Vue.set(this.tree.elementsID, address, true);
      element.id = address;
    }
  }
}

export { Address, AddressBlock, TreeStruct, Connection };
