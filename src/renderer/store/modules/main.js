const state = {
  activeComponent: null,
  bufferCopy: null,
  mode: {
    isMain: true,
    isPlugin: false,
    isVisioPlugin: false,
    isOpenProject: false,
    isTreeAddress: true,
    isAddDevice: false,
    isConfigBlock: false,
    isMonitorBus: false,
  },
  config: {
    autoSave: true,
    timerAutoSave: 60000,
  },
  projects: {
    currentProject: null,
    lastProject: null,
    projectsList: [],
  },
  /* types: newDevices */
  drag: {
    data: {},
    type: null,
  },
  dataTypes: [
    {
      label: 'Дискретное значение',
      info: '1 byte',
      value: 1,
    },
    {
      label: 'Беззнаковое число',
      info: '1 byte',
      value: 2,
    },
    {
      label: 'Знаковое число',
      info: '1 byte',
      value: 3,
    },
    {
      label: 'Управление',
      info: '1 byte',
      value: 4,
    },
    {
      label: 'Беззнаковое число',
      info: '2 byte',
      value: 5,
    },
    {
      label: 'Знаковое число c запятой',
      info: '4 byte',
      value: 6,
    },
    {
      label: 'HEX color',
      info: '4 byte',
      value: 7,
    },
    {
      label: 'Время и день',
      info: '4 byte',
      value: 8,
    },
    {
      label: 'Дата',
      info: '4 byte',
      value: 9,
    },
    {
      label: 'Беззнаковое число',
      info: '4 byte',
      value: 10,
    },
    {
      label: 'Знаковое число',
      info: '4 byte',
      value: 11,
    },
  ],
  msgModbusTypes: [
    {
      label: 'Read Coil Status',
      value: 1,
    },
    {
      label: 'Read Input Status',
      value: 2,
    },
    {
      label: 'Read Holding Registers',
      value: 3,
    },
    {
      label: 'Read Input Registers',
      value: 4,
    },
    {
      label: 'Force Single Coil',
      value: 5,
    },
    {
      label: 'Preset Single Register',
      value: 6,
    },
    {
      label: 'Force Multiple Coils',
      value: 15,
    },
    {
      label: 'Preset Multiple Registers',
      value: 16,
    },
  ],
};

const mutations = {
  // Создаем проект
  CREAT_NEW_PROJECT(state, { data, open }) {
    state.projects.projectsList.push(data.info);
    if (open) {
      state.projects.currentProject = data.info;
      state.mode.isOpenProject = true;
    }
  },
  // Иницилизация State при запуске системы
  INIT_STATE(state, data) {
    if (data.config) state.config = data.config;
    if (data.projects) state.projects = data.projects;
  },
  // Открыть проект
  OPEN_PROJECT_MAIN(state, data) {
    state.projects.currentProject = data;
    state.mode.isOpenProject = true;
  },
  // Сохраняем дату изменения проект при нажати кнопки сохранить.
  SAVE_DATE(state, data) {
    state.projects.currentProject.editDate = data;
  },
  CLOSE_PROJECT(state) {
    state.projects.lastProject = state.projects.currentProject;
    state.projects.currentProject = null;
    state.mode.isOpenProject = false;
  },
  TOGGLE_PLUGIN(state) {
    state.mode.isMain = !state.mode.isMain;
    state.mode.isPlugin = !state.mode.isPlugin;
  },
  TOGGLE_VISIO_PLUGIN(state) {
    state.mode.isMain = !state.mode.isMain;
    state.mode.isVisioPlugin = !state.mode.isVisioPlugin;
  },
  TOGGLE_TREE_ADDRESS(state) {
    state.mode.isTreeAddress = !state.mode.isTreeAddress;
  },
  TOGGLE_ADD_DEVICE(state) {
    state.mode.isAddDevice = !state.mode.isAddDevice;
  },
  TOGGLE_MONITOR_BUS(state) {
    state.mode.isMonitorBus = !state.mode.isMonitorBus;
  },
  SET_CONFIG_BLOCK(state, status = false) {
    state.mode.isConfigBlock = status;
  },
  SAVE_NAME_PROJECT(state, data) {
    data.el.name = data.newName;
  },
  // Удаляем проект из хранилища
  DELETE_PROJECT(state, data) {
    const index = state.projects.projectsList.indexOf(data);
    if (index !== -1) state.projects.projectsList.splice(index, 1);
    if (state.projects.lastProject) {
      if (state.projects.lastProject.name === data.name) state.projects.lastProject = null;
    }
  },
  DRAG_GLOBAL(state, data) {
    state.drag = data;
  },
  DRAG_GLOBAL_STOP(state) {
    state.drag = {
      data: {},
      type: null,
    };
  },
  SELECT_ACTIVE_COMPONENT(state, data) {
    if(data !== state.activeComponent) {
      state.activeComponent = data;
      state.bufferCopy = null;
    }
  },
  SET_COPY_BUFFER(state, data) {
    state.bufferCopy = _.cloneDeep(data);
  }
};

const actions = {};

export default {
  state,
  mutations,
  actions,
};
