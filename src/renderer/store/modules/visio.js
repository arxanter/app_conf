import Vue from 'vue';
import widgetsList from '../common/widgetsList.js';
import iconsArray from '../common/iconsArray.js';
// !!! удалить после импорта в плагин;
const _ = require('lodash');
// !!! END

// Func with generate short UID
const generateUID = () => {
  // I generate the UID from two parts here
  // to ensure the random number provide enough bits.
  let firstPart = (Math.random() * 46656) | 0; //eslint-disable-line
  let secondPart = (Math.random() * 46656) | 0; //eslint-disable-line
  firstPart = `000${firstPart.toString(36)}`.slice(-3);
  secondPart = `000${secondPart.toString(36)}`.slice(-3);
  return firstPart + secondPart;
};

const replaceArray = (data, menuItem, indexColumn) => {
  if (indexColumn !== undefined) {
    Vue.set(menuItem.widgets, indexColumn, data);
  } else menuItem.widgets = data;
};

const generateTemplate = (value) => {
  const template = {
    name: 'BaseName',
    template: value,
    menuItems: [],
    menuSystems: [],
    menuPlaces: []
  }
  return template;
}

export default {
  state: {
    mode: 'develop',
    editElement: null,
    activeMenu: null,
    iconsArray,
    statusList: [],
    data: {},
    widgetsList,
  },

  getters: {},
  mutations: {
    SET_MODE_VISIO(state, mode) {
      state.mode = mode;
    },
    END_CONFIG_VISIO(state) {
      state.data = {};
      state.activeMenu = null;
      state.editElement = null;
    },
    SET_PROJECT_DATA_VISIO(state, data) {
      Vue.set(state, 'data', data);
    },
    SET_STATUS_LIST_VISIO(state, data) {
      Vue.set(state, 'statusList', data);
    },
    SET_STATUS_VALUE(state, { status, value }) {
      let correctValue = value;
      if (status.dataType === 6) correctValue = (+value).toFixed(2);
      console.log(`Команда установить - ${value} в ${status}`);
      const item = state.statusList.find(el => el.status === status);
      if (item) {
        Vue.set(item, 'value', correctValue);
      } else if (status !== null && status !== undefined) state.statusList.push({ status, value: correctValue });
    },
    REPLACE_WIDGET(state, { data, indexColumn }) {
      const menuItem = state.activeMenu;
      //  !!! Костыль для недублирования объектов, чтобы они все были уникальны.
      const currectData = data.map(el => {
        if (el !== state.editElement) return _.cloneDeep(el);
        return el;
      });
      // Костыль END
      replaceArray(currectData, menuItem, indexColumn);
    },
    REMOVE_WIDGET(state, { data, menuUID, indexColumn }) {
      const menuItem = state.data.menuItems.find(el => el.uid === menuUID);
      const arrayItems = indexColumn !== undefined ? menuItem.widgets[indexColumn] : menuItem.widgets;
      const newArray = arrayItems.filter(el => el !== data);
      if (data === state.editElement) state.editElement = null;
      replaceArray(newArray, menuItem, indexColumn);
    },
    SELECT_EDIT(state, data) {
      Vue.set(state, 'editElement', data);
    },
    SELECT_ACTIVE_MENU(state, data) {
      Vue.set(state, 'activeMenu', data);
    },
    CHANGE_SETTINGS(state, settings) {
      Vue.set(state.editElement, 'settings', settings);
    },
    CHANGE_ADDRESSES(state, arrayAddresses) {
      Vue.set(state.editElement, 'addresses', arrayAddresses);
    },
    REMOVE_ITEM_MENU(state, { item, parent } = {}) {
      const array = parent ? parent.subItems : state.data.menuItems;
      const indexItem = array.indexOf(item);
      if (indexItem !== -1) {
        array.splice(indexItem, 1);
        if (item === state.activeMenu) state.activeMenu = null;
        if (item === state.editElement) state.editElement = null;
      }
    },
    ADD_ITEM_MENU(state, { parent, isChildItem = false } = {}) {
      // Creat new UID for menu Item and check uniqueness
      const array = parent || state.data.menuItems;
      const creatUID = () => {
        let uid = generateUID();
        if (array.indexOf(uid) !== -1) uid = creatUID();
        return uid;
      };
      const uid = creatUID();
      const newMenuItem = {
        settings: {
          name: 'Заголовок',
          templateType: 'ColumnsTemplate',
          icon: 'home',
          isSubItems: false,
          isChildItem,
        },
        type: 'menuItem',
        widgets: [[], [], []],
        subItems: [],
        uid,
      };
      // Set new item Menu
      array.push(newMenuItem);
    },
    CHANGE_VISIO_PLUGIN(state, value) {
      const newData = generateTemplate(value);
      if (newData) {
        Object.keys(newData).forEach(name => {
          Vue.set(state.data, name, newData[name]);
        })
        Vue.set(state, 'editElement', null);
        Vue.set(state, 'activeMenu', null);
      }
    },
    SAVE_USERS_LIST_VISIO(state, value) {
      state.data.users = value;
    }
  },
  actions: {},
};
