import Vue from 'vue';
import Vuex from 'vuex';
import { calcCRC32 } from '../../../main/service/workCRC';
import { Address, AddressBlock, TreeStruct, Connection } from '../classes/ObjectClasses';
Vue.use(Vuex);

/* State  */
const state = {
  devices: new TreeStruct('Устройства', 'devices', 'Устройство'),
  addresses: new TreeStruct('Групповые адреса', 'addresses', 'Адрес'),
  blocks: new TreeStruct('Доступные модули', 'blocks', 'Блок'),
  findedDevices: [],
};

/* Mutations  */
const mutations = {
  OPEN_PROJECT(state, data) {
    Vue.set(state.devices, 'tree', data.devices);
    Vue.set(state.addresses, 'tree', data.addresses);
    // Добавляем прототип дерева к внутренней структуре блоков
    _.flatten(state.devices.tree.elements).forEach(device => {
      // Если элемент логический контроллер, обновляем/воссоздаем прототип структуры с функц. блоками.
      if (device.params.isFBD) {
        Object.setPrototypeOf(device.structBlocks, new TreeStruct());
        Vue.set(device, 'blocks', _.flatten(device.structBlocks.tree.elements));
      }
    });
    // Обновляем связи адреса и Графического блока адреса
    const deviceFnBlocks = _.flatten(state.devices.tree.elements).filter(el => el.addressBlocks);
    deviceFnBlocks.forEach(device => {
      device.addressBlocks.forEach(addressBlock => {
        addressBlock.address = _.flatten(state.addresses.tree.elements).find(el => el.id === addressBlock.address.id);
      });
    });
  },
  // Изменения статуса открытия папки
  TOGGLE_OPEN_FOLDER_TREE(state, element) {
    element.isOpen = !element.isOpen;
  },
  // Установка статуса открытия папки.
  SET_OPEN_FOLDER_TREE(state, { element, status }) {
    element.isOpen = status;
  },
  COLLAPSE_FOLDER(state, { struct, folder }) {
    struct.collapseFolder(folder);
  },
  TOGGLE_OPEN_BLOCK(state, { block, value }) {
    block.props.isOpen = value;
  },
  // Select single element tree
  SELECT_ELEMENT_TREE(state, { struct, element }) {
    Vue.set(struct.props, 'changeElement', null);
    struct.props.changeElement = element;
  },
  REPLACE_ELEMENT_TREE(state, { struct, element, parentID, dropElement, isFirstHalfValue }) {
    // Проверка, чтобы не происходила вставка родительской папки в дочернюю папку.
    const arrayPosition = element.type === 'element' ? struct.tree.elements : struct.tree.folders;
    const oldIndex = arrayPosition[element.parentID].indexOf(element);
    arrayPosition[element.parentID].splice(oldIndex, 1);
    element.parentID = parentID;

    // Случай когда происходит сортировка элементов Tree Struct
    if (dropElement) {
      let index = struct.getIndexElement(dropElement);
      if (!isFirstHalfValue) index += 1;
      arrayPosition[parentID].splice(index, 0, element);
      // Если просто необходимо вставить в родительский массив
    } else {
      arrayPosition[parentID].push(element);
    }
    // Обновляем tag folder
    if (element.type === 'folder') struct.updateTagFolder(element);
  },
  MOVE_ELEMENT_TREE(state, { struct, element, direction }) {
    const arrayPosition = element.type === 'element' ? struct.tree.elements : struct.tree.folders;
    const oldIndex = arrayPosition[element.parentID].indexOf(element);
    // Проверяем возможна ли перестановка.
    if ((direction === 'up' && oldIndex !== 0) || (direction === 'down' && oldIndex < arrayPosition.length)) {
      // Удаляем элемент с текущего его положения
      arrayPosition[element.parentID].splice(oldIndex, 1);
      // Вставляем в новое положение
      if (direction === 'up') arrayPosition[element.parentID].splice(oldIndex - 1, 0, element);
      else if (direction === 'down') arrayPosition[element.parentID].splice(oldIndex + 1, 0, element);
    }
  },
  // Creat new folder tree
  ADD_FOLDER_TREE(state, { struct, parent, name }) {
    struct.addFolder(name, parent);
  },
  ADD_ADDRESS_TREE(state, { name, id, parent = null, bus = 1 }) {
    state.addresses.addElement(new Address(name, bus), parent, id);
  },
  ADD_DEVICE(state, { device, parent }) {
    /* Рудимент. Скорее всего это лишнее.
    device.params.mac = '00:00:00:00:00:00';
    device.params.isOnline = false;
    */
    if (device.params.isFBD) {
      const folderAddresses = state.addresses.addFolder(device.fnType, null, true);
      device.folderAddressesID = folderAddresses.id;
      Vue.set(device, 'structBlocks', new TreeStruct('Функциональные блоки', 'blocks', 'Блок'));
      Vue.set(device, 'connections', []);
      Vue.set(device, 'blocks', []);
      Vue.set(device, 'addressBlocks', []);
    }
    state.devices.addElement(device, parent);
  },
  INSERT_BLOCK_TREE(state, { block, struct, parent, left = 0, top = 0 }) {
    block.id = null;
    block.props.left = left;
    block.props.top = top;
    block.props.isOpen = false;
    block.subtype = 'block';
    block.clearVariables = false;
    const createdBlock = struct.addElement(block, parent);
    // Calc and set crc32 to block
    const dataForHash = Date.now().toString(16) + createdBlock.fnType + createdBlock.fnTypeValue + createdBlock.id;
    createdBlock.hashUID = calcCRC32(dataForHash);
    createdBlock.clearVariables = false;
  },
  // Delete element tree
  REMOVE_ELEMENT_TREE(state, { struct, element }) {
    if (element.type === 'folder') struct.deleteFolder(element);
    if (element.type === 'element') struct.deleteElement(element);
    // Если удаляемый элемент устройство - удаляем его папку локальных адресов
    if (element.folderAddressesID) {
      const folder = state.addresses.getFolderByID(element.folderAddressesID);
      state.addresses.deleteFolder(folder);
    }
    // Убираем из changeElement удаленный объект.
    if (struct.props.changeElement === element) struct.props.changeElement = null;
  },
  REMOVE_BLOCK_FROM_ARRAY(state, { device, block }) {
    const index = device.blocks.findIndex(el => el === block);
    device.blocks.splice(index, 1);
  },
  // Save folder after edit
  SAVE_EDIT_FOLDER(state, { struct, element, name }) {
    struct.editFolder(element, name);
  },
  // Save element after edit
  EDIT_ELEMENT_TREE(state, { struct, element, name, address }) {
    const oldName = element.name;
    struct.editElement(element, name, address);
    // Проверяем связи в адресах, если есть с этим блоком или устройством, заменяем их.
    if (oldName !== name) {
      if (element.subtype === 'device') {
        _.flatten(state.addresses.tree.elements).forEach(el => {
          el.sockets.forEach(socket => {
            if (socket.device.id === element.id) socket.device.name = element.name;
          });
        });
      }
      if (element.subtype === 'block') {
        _.flatten(state.addresses.tree.elements).forEach(el => {
          el.sockets.forEach(socket => {
            if (socket.block.id === element.id) socket.block.name = element.name;
          });
        });
      }
    }
  },
  // Сохранение настроек устройства
  SAVE_SETTINGS_DEVICE(state, { device, settings }) {
    device.settings = settings;
  },
  CLOSE_PROJECT(state) {
    state.devices = new TreeStruct('Устройства', 'devices', 'Устройство');
    state.addresses = new TreeStruct('Групповые адреса', 'addresses', 'Адрес');
  },
  // Add  socket <-> address
  INSERT_LINK(state, { socket, address }) {
    // Подстраховка при некорректных данных socket
    if (socket.direction === undefined) socket.direction = socket.object.direction;
    // Add new data to address.sockets and object.addresses. The address stores the socket Object, but object stores only address.id
    // Упрощаем структуру сокета, для хранения в адресе
    const socketData = _.clone(socket);
    socketData.block = { id: socket.block.id, name: socket.block.name };
    socketData.device = { id: socket.device.id, name: socket.device.name };
    address.sockets.push(socketData);
    // Checking. Is it necessary to set this address to a specific position
    if (socket.indexSetAddress !== undefined) {
      socket.object.addresses.splice(socket.indexSetAddress, 0, address.id);
      delete socket.indexSetAddress;
    } else socket.object.addresses.push(address.id);
    // !!! Not an elegant solution. Think about refactoring.
    socket.object.addresses = _.uniq(socket.object.addresses);
  },
  // Breaking the link
  REMOVE_LINK(state, { socket, address }) {
    const device = state.devices.getElementByID(socket.device.id);
    // Находим требуемый блок и объект
    const block = device.blocks.find(el => el.id === socket.block.id);
    const object = block.objects.find(el => el.name === socket.object.name);
    // Индексы расположения элементов для удаления.
    const indexAddress = object.addresses.indexOf(address.id);
    const indexSocket = address.sockets.findIndex(
      el =>
        el.device.id === socket.device.id && el.block.id === socket.block.id && el.object.name === socket.object.name
    );
    // Удаляем связь объекта и адреса в обоих структурах.
    if (indexAddress !== -1) object.addresses.splice(indexAddress, 1);
    if (indexSocket !== -1) address.sockets.splice(indexSocket, 1);
  },
  // Add graphical link path
  ADD_LINK_PATH(state, { sockets, device, address }) {
    const dataCreate = {
      source: sockets.find(el => el.direction === 'output'),
      target: sockets.find(el => el.direction === 'input'),
      address,
    };
    const connection = new Connection(dataCreate);
    // Проверка на наличие связи между данными объектами
    const isInclude = device.paths.find(
      el => el.target.DOM_ID === connection.target.DOM_ID && el.source.DOM_ID === connection.source.DOM_ID
    );
    if (!isInclude) device.paths.push(connection);
  },
  // Save path position
  SAVE_LINK_PATH_POSITION(state, { path, position }) {
    path.position = position;
  },
  // Remove graphical link path
  REMOVE_LINK_PATH(state, { address, socket }) {
    const device = state.devices.getElementByID(socket.device.id);
    const index = device.paths.findIndex(path => {
      const pathPoint = socket.direction === 'input' ? path.target : path.source;
      if (socket.block.id === pathPoint.blockID && address.id === path.addressID && socket.block.objects)
        return socket.object.name === socket.block.objects[pathPoint.socketIndex].name;
      return false;
    });
    if (index !== -1) device.paths.splice(index, 1);
  },
  // Создаем список видов блоков для возможности их добавления в плагине.
  CREAT_BLOCKS_LIST(state, blocks) {
    Object.keys(blocks).forEach(blocksType => {
      const folder = state.blocks.addFolder(blocks[blocksType].params.name.ru);
      // Записываем базовые параметры, которые не храняться в json файле
      const blocksArray = [];
      Object.keys(blocks[blocksType]).forEach(key => {
        if (key !== 'params') blocksArray.push(blocks[blocksType][key]);
      });
      blocksArray.forEach(el => {
        el.type = 'element';
        el.subtype = 'newBlock';
        el.props = {
          top: 0,
          left: 0,
          script: 1,
        };
      });
      blocksArray.forEach(element => {
        state.blocks.addElement(element, folder);
      });
    });
  },
  UPDATE_ADDRESS_DATATYPE(state, { address, dataType = null }) {
    // Update address dataType and address dataType in knx table conform
    function updateAddressDataType(address, dataType) {
      address.dataType = dataType;
      _.flatten(state.devices.tree.elements).forEach(device => {
        if (device.knx)
          device.knx.tableConform.forEach(item => {
            if (item.addressID === address.id) {
              item.isLockDataType = !!dataType;
              item.dataType = dataType;
            }
          });
      });
    }
    // Check address dataType
    if (address.bus === 1) {
      if (address.sockets.length === 0) updateAddressDataType(address, null);
      if (dataType && address.dataType !== dataType) updateAddressDataType(address, dataType);
    }
  },
  /**
   * Events from plugin
   *
   *
   */
  // Save block position from plugin
  SAVE_BLOCK_POSITION(state, { element, top, left }) {
    element.props.left = left;
    element.props.top = top;
  },
  // Set new settings block from interface
  SET_SETTINGS_BLOCK(state, { device, blockID, data }) {
    let block = null;
    if (device.structBlocks) {
      const arrayBlocks = _.flatten(device.structBlocks.tree.elements);
      block = arrayBlocks.find(el => el.id === blockID);
    } else {
      block = device.blocks.find(el => el.id === blockID);
    }
    Vue.set(block, 'settings', data.settings);
    Vue.set(block, 'objects', data.objects);
    Vue.set(block, 'clearVariables', data.clearVariables);
  },
  // Combine block from treeStruct to Array
  COMBINE_BLOCKS(state, device) {
    Vue.set(device, 'blocks', _.flatten(device.structBlocks.tree.elements));
  },
  // Добавляем блок адреса в плагин устройства
  INSERT_EXTERNAL_ADDRESS_BLOCK(state, { address, device, top = 0, left = 0 }) {
    if (device.addressBlocks.find(el => el.address.id === address.id) === undefined) {
      const block = new AddressBlock(address, top, left);
      device.addressBlocks.push(block);
    }
  },
  // Удаляем блок адреса из плагина
  REMOVE_EXTERNAL_ADDRESS_BLOCK(state, { address, device }) {
    const index = device.addressBlocks.findIndex(el => el.address.id === address.id);
    if (index !== -1) device.addressBlocks.splice(index, 1);
  },
  // Select first address at socket. First address = main address
  REPLACE_ADDRESSES_OBJECT(state, { object, addresses }) {
    Vue.set(object, 'addresses', addresses);
  },
  UPDATE_ACTUAL_SETTINGS_DEVICE(state, device) {
    const settings = state.devices.getElementByID(device.id).settings;
    settings.actualIP = settings.ip;
    settings.actualPortConfigTCP = settings.portConfigTCP;
  },
  // Save MODBUS config of the device
  SAVE_MODBUS_CONFIG(state, { device, settings }) {
    device.modbus = settings;
  },
  // Save found and added 1-wire sensors in device
  SAVE_ONEWIRE_DEVICES(state, { device, findSensors, saveSensors }) {
    device.oneWire.findSensors = findSensors;
    device.oneWire.saveSensors = saveSensors;
  },
  // Save device knxTable
  SAVE_ITEM_KNX_TABLE(state, { device, index, value }) {
    if (index !== undefined) Vue.set(device.knx.tableConform, index, value);
    else device.knx.tableConform.push(value);
  },
  REMOVE_ITEM_KNX_TABLE(state, { device, index }) {
    device.knx.tableConform.splice(index, 1);
  },
  SET_FINDED_DEVICE(state, device) {
    if (state.findedDevices.find(el => el.mac === device.mac) === undefined) {
      state.findedDevices.push(device);
      Vue.set(state, 'findedDevices', state.findedDevices.sort((a, b) => a.ip - b.ip));
    }
    // Если такое устройство есть в проект, обнавляем его статус
    const item = _.flatten(state.devices.tree.elements).find(el => el.params.mac === device.mac);
    if (item) {
      item.params.actualIP = device.ip;
      item.params.isOnline = true;
    }
  },
  CLEAR_FINDED_DEVICES(state) {
    state.findedDevices = [];
  },
  SET_STATUS_DEVICES(state) {
    _.flatten(state.devices.tree.elements)
      .filter(el => el.params.mac !== '00:00:00:00:00:00')
      .forEach(device => {
        const item = state.findedDevices.find(item => item.mac === device.params.mac);
        if (item === undefined) {
          device.params.actualIP = '-';
          device.params.isOnline = false;
        } else {
          device.params.actualIP = item.ip;
          device.params.isOnline = true;
        }
      });
  },
  SET_STATUS_DEVICE(state, { device, status = false }) {
    device.params.isOnline = status;
    state.findedDevices = state.findedDevices.filter(el => el.mac !== device.params.mac);
  },
  SET_COMBINE_DEVICE(state, { device, data }) {
    device.params.mac = data.mac;
    device.params.actualIP = data.ip;
    device.settings.ip = data.ip;
    device.params.isOnline = true;
  },
  REMOVE_COMBINE_DEVICE(state, device) {
    device.params.mac = '00:00:00:00:00:00';
    device.params.actualIP = '-';
    device.params.isOnline = false;
  },
};

/**
 *
 *
 *
 *
 *
 * ACTION
 */
const actions = {
  /**
   *
   */
  async ADD_ELEMENT_TREE({ commit }, { struct, name, parent, type }) {
    // !!! Cлучай строго для адресов, можно расширить позже, загнав сюда subtype разных типов
    if (type === 'element') commit('ADD_ADDRESS_TREE', { name, id: null, parent, bus: 1 });
    else if (type === 'folder') commit('ADD_FOLDER_TREE', { struct, name, parent });
    else throw new Error('неизвестный тип');
    return 'Элемент успешно добавлен';
  },
  async ADD_BLOCK_TREE({ commit }, data) {
    const blockLength = _.flatten(data.struct.tree.elements).length;
    if (blockLength >= data.device.params.maxBlocks) throw new Error('Достигнуто максимальное количество блоков');
    else commit('INSERT_BLOCK_TREE', data);
    return 'Блок успешно добавлен';
  },
  async ADD_EXTERNAL_ADDRESS_BLOCK({ commit }, data) {
    if (data.device.addressBlocks.find(el => el.address.id === data.address.id) !== undefined)
      throw new Error('Адрес уже есть в плагине');
    commit('INSERT_EXTERNAL_ADDRESS_BLOCK', data);
    return 'Блока адреса успешно добавлен';
  },
  /**
   *
   * socketBlock: socket: { block:{}, object: {}, device: {}, dataType, direction, type: 'blockSocket',  }
   * socketAddress: socket: { address: {}, dataType, direction, type: 'addressSocket' }
   */
  /**
   *
   * Refactoring is required. Very hard to read and complicated code.
   * Divide into 2 cases. Maybe 2 action
   * func. block <-> address and func. block <-> func. block
   */

  async ADD_LINK_PLUGIN({ dispatch, commit, state }, sockets) {
    let address = null;
    const addressSocket = sockets.find(el => el.type === 'addressSocket');
    const socketOutput = sockets.find(socket => socket.direction === 'output' && socket.type === 'blockSocket');
    const socketInput = sockets.find(socket => socket.direction === 'input' && socket.type === 'blockSocket');
    const blockSockets = sockets.filter(el => el.type === 'blockSocket');
    // Проверка наличия хотя бы одного функционального блока
    if (!sockets.find(socket => socket.type === 'blockSocket')) throw new Error('Нельзя соединять адрес с адресом');
    // Проверка на корректность типа данных объектов и адреса.
    if (sockets[0].block === sockets[1].block) throw new Error('Нельзя соединять объекты одного блока');
    // Проверка на корректность типа данных объектов и адреса.
    if (sockets[0].dataType && sockets[1].dataType && sockets[0].dataType !== sockets[1].dataType)
      throw new Error('Различный тип данных сокетов');
    // Проверка на напрвленность соединяемых сокетов
    if (sockets[0].direction === sockets[1].direction) throw new Error('Нельзя соединять сокеты одного направления');
    // Проверка на избыточное кол-во адресов в сокете input
    if (socketInput)
      if (socketInput.object.addresses.length >= 14 && !socketInput.object.addresses.includes(address))
        throw new Error('Предельное количество адресов в сокете Input');
    /**
     * Ищем addressSocket или создаем новый address.
     */
    // Если один из сокетов - адрес, новый адрес создавать не нужно.
    if (addressSocket) address = addressSocket.address;
    // Если у сокета output есть адрес, просто берем его. Не создаем новый.
    else if (socketOutput.object.addresses.length) {
      const addressID = socketOutput.object.addresses[0];
      address = state.addresses.getElementByID(addressID);
      // Если у сокетов нет адреса, создаем новый локальный адрес. bus = 0;
    } else {
      const newID = state.addresses.getElementID;
      const parentID = sockets.find(socket => socket.type === 'blockSocket').device.folderAddressesID;
      commit('ADD_ADDRESS_TREE', { name: '', id: newID, parent: state.addresses.getElementByID(parentID), bus: 0 });
      address = state.addresses.getElementByID(newID);
    }

    /**
     * Добавляем связь для данных сокетов.
     */
    const promises = blockSockets.map(async socket => {
      await dispatch('ADD_LINK', { socket, address, isNeedLink: blockSockets.length !== 2 });
    });
    await Promise.all(promises);
    // Добавляем path для связи. Если мы соединяем 2 сокета функц.блока
    if (blockSockets.length > 1) commit('ADD_LINK_PATH', { sockets, device: blockSockets[0].device, address });

    // !!! Что это за монстр???
    if (socketOutput && socketInput) {
      if (socketOutput.object.addresses.includes(address.id) && socketInput.object.addresses.includes(address.id))
        return null;
    } else if (socketOutput) {
      if (socketOutput.object.addresses.includes(address.id)) return null;
    } else if (socketInput) {
      if (socketInput.object.addresses.includes(address.id)) return null;
    }

    return 'Связь успешно добавлена';
  },
  /**
   * socketBlock: socket: { block:{}, object: {}, device: {}, type: 'blockSocket'}
   * isNeedLink - нужно ли строить графическую линию
   */
  async ADD_LINK({ commit, dispatch, state }, { socket, address, isNeedLink = true }) {
    // Проверка на соед. input с output одного блока
    address.sockets
      .filter(el => el.block.id === socket.block.id)
      .forEach(el => {
        if (el.object.direction !== socket.object.direction)
          throw new Error('Нельзя соединять объекты Input и Output одного блока');
      });
    // Проверка, существует ли уже эта связь.
    if (socket.object.addresses.includes(address.id)) return null;
    // Проверка на тип данных объекта.
    if (address.dataType !== socket.object.dataType)
      if (address.dataType !== null) throw new Error('Различные типы данных объектов');
    // Проверка на предельное кол-во адресов в input
    if (socket.object.direction === 'input')
      if (socket.object.addresses.length >= 14) throw new Error('Предельное количество адресов в сокете Input');
    if (socket.object.direction === 'output' && socket.object.addresses.length && address.bus !== 0)
      if (socket.object.addresses[0].bus !== 0)
        throw new Error('Нельзя добавлять больше одного адреса в объект Output');
    // Добавляем блок адреса в плагин устройства.
    const device = state.devices.getElementByID(socket.device.id);
    /**
     * !!! Требует рефактор из-за сложности восприятия
     */
    if (device.params.isFBD) {
      const notAddressInDevice = device.addressBlocks.find(el => el.address.id === address.id) === undefined;
      if (address.bus === 1 && notAddressInDevice) dispatch('ADD_EXTERNAL_ADDRESS_BLOCK', { address, device });
      const addressSocket = {
        type: 'addressSocket',
        address,
        direction: socket.direction === 'input' ? 'output' : 'input',
      };
      /**
       * Если сокет имеет адрес и он локальный, а новый адрес внешний.
       * Возвращаем сокеты локального адреса и удаляем его.
       * К этим сокетам добавляем связь с внешним адресом.
       */
      if (socket.object.direction === 'output' && socket.object.addresses.length && address.bus !== 0) {
        const addressOld = state.addresses.getElementByID(socket.object.addresses[0]);
        /** releaseSockets - sockets which need replace from localAddress to newAddress,
         * indexAddressPosition ([null, 2, null, 10, null]) - index position localAddress for insert newAddres at that index
         */
        const releaseSockets = await dispatch('RELEASE_SOCKETS_FOR_REPLACE', addressOld);
        if (releaseSockets) {
          releaseSockets.forEach(_socket => {
            const addressSocket = {
              type: 'addressSocket',
              address,
              direction: _socket.direction === 'input' ? 'output' : 'input',
            };
            commit('INSERT_LINK', { socket: _socket, address });
            commit('UPDATE_ADDRESS_DATATYPE', { address, dataType: _socket.object.dataType });
            commit('ADD_LINK_PATH', { sockets: [_socket, addressSocket], device: _socket.device, address });
          });
        }
        // A simple case. Transformation is not needed
      } else {
        commit('INSERT_LINK', { socket, address });
        commit('UPDATE_ADDRESS_DATATYPE', { address, dataType: socket.object.dataType });
        if (isNeedLink && device.params.isFBD)
          commit('ADD_LINK_PATH', { sockets: [socket, addressSocket], device: socket.device, address });
      }
    } else {
      commit('INSERT_LINK', { socket, address });
      commit('UPDATE_ADDRESS_DATATYPE', { address, dataType: socket.object.dataType });
    }

    return 'Связь успешно добавлена';
  },
  /**
   *
   *
   */
  RELEASE_SOCKETS_FOR_REPLACE({ dispatch }, address) {
    const releaseSockets = [];
    address.sockets.forEach(el => {
      let socket = el;
      // If socket.object is 'input' and have more than 1 address -> remember this index.
      if (el.object.direction === 'input' && el.object.addresses.length > 1) {
        socket = Object.assign({}, el);
        socket.indexSetAddress = el.object.addresses.indexOf(address.id);
      }
      releaseSockets.push(socket);
    });
    // Delete link with old address.
    address.sockets.forEach(socket => dispatch('DELETE_LINK', { address, socket }));
    return releaseSockets;
  },
  /**
   *
   */
  async DELETE_LINK({ state, commit, dispatch }, { address, socket }) {
    commit('REMOVE_LINK', { address, socket });
    const device = state.devices.getElementByID(socket.device.id);
    if (device.params.isFBD) commit('REMOVE_LINK_PATH', { address, socket });
    // Удаляем локальный адрес, если у него нет связей или есть связь только одного направления.
    if (address.bus === 0) {
      const notInputSocket = !address.sockets.find(el => el.object.direction === 'input');
      const notOutputSocket = !address.sockets.find(el => el.object.direction === 'output');
      if (notInputSocket || notOutputSocket) {
        await dispatch('DELETE_ADDRESS', address);
      }
    }
    if (address.bus === 1) commit('UPDATE_ADDRESS_DATATYPE', { address });
    // Удаляем графические связи, связанные с этим сокетом.
    return 'Связь успешно разорвана';
  },
  /**
   *
   */
  async DELETE_ADDRESS({ state, commit, dispatch }, address) {
    // Чистим связи Адрес - объект
    // If we delete external address, check all device with fnBlocks and delete blocks with that address
    const deviceAddressBlocks = _.flatten(state.devices.tree.elements).filter(el => el.addressBlocks);
    deviceAddressBlocks.forEach(device => {
      if (device.addressBlocks.find(el => el.address.id === address.id))
        commit('REMOVE_EXTERNAL_ADDRESS_BLOCK', { device, address });
    });
    const promises = address.sockets.map(async socket => {
      await dispatch('DELETE_LINK', { address, socket });
    });
    await Promise.all(promises);
    // Удаляем адрес из структуры
    commit('REMOVE_ELEMENT_TREE', { element: address, struct: state.addresses });
    return 'Адрес успешно удален';
  },
  /**
   *
   */
  async DELETE_EXTERNAL_ADDRESS_PLUGIN({ commit, dispatch }, { address, device }) {
    const sockets = address.sockets.filter(socket => socket.device.id === device.id);
    commit('REMOVE_EXTERNAL_ADDRESS_BLOCK', { address, device });
    const outputSockets = address.sockets.filter(el => el.object.direction === 'output' && el.device.id === device.id);
    const inputSockets = address.sockets.filter(el => el.object.direction === 'input' && el.device.id === device.id);
    sockets.forEach(socket => {
      dispatch('DELETE_LINK', { address, socket });
    });
    outputSockets.forEach(socket => {
      inputSockets.forEach(input => dispatch('ADD_LINK_PLUGIN', [socket, input]));
    });

    return 'Адрес успешно удален из плагина';
  },
  /**
   *
   */
  async DELETE_FUNC_BLOCK({ state, commit, dispatch }, { block, device }) {
    const globalPromises = block.objects.map(async object => {
      const promises = [...object.addresses].map(async addressID => {
        const address = state.addresses.getElementByID(addressID);
        await dispatch('DELETE_LINK', { address, socket: { object, device, block, direction: object.direction } });
      });
      await Promise.all(promises);
    });
    await Promise.all(globalPromises);
    // Удаляем блоки из структуры устройства.

    if (device.structBlocks) commit('REMOVE_ELEMENT_TREE', { element: block, struct: device.structBlocks });
    commit('REMOVE_BLOCK_FROM_ARRAY', { block, device });
    return 'Блок успешно удален';
  },
  /**
   *
   */
  async DELETE_DEVICE({ state, commit, dispatch }, device) {
    const promises = device.blocks.map(async block => {
      await dispatch('DELETE_FUNC_BLOCK', { block, device });
    });
    await Promise.all(promises);
    commit('REMOVE_ELEMENT_TREE', { element: device, struct: state.devices });
    return 'Устройство успешно удалено';
  },
  /**
   *
   */
  async DELETE_FOLDER_TREE({ commit, dispatch }, { element, struct, device = null }) {
    const childFolders = struct.tree.folders[element.id] || [];
    const childElements = struct.tree.elements[element.id] || [];
    const promisesChildFolders = childFolders.map(async folder => {
      await dispatch('DELETE_FOLDER_TREE', { element: folder, struct });
    });
    await Promise.all(promisesChildFolders);
    const promises = childElements.map(async el => {
      if (el.subtype === 'address') await dispatch('DELETE_ADDRESS', el);
      if (el.subtype === 'block') await dispatch('DELETE_FUNC_BLOCK', { block: el, device });
      if (el.subtype === 'device') await dispatch('DELETE_DEVICE', el);
    });
    await Promise.all(promises);
    commit('REMOVE_ELEMENT_TREE', { element, struct });
    return 'Папка успешно удалена';
  },
  /**
   *
   */
  async SAVE_SETTINGS_BLOCK({ state, commit, dispatch }, { newBlock, oldBlock, device }) {
    // Пробегаемся по объектам
    const globalPromises = newBlock.objects.map(async (object, index) => {
      // Если у объект стал неактивен или изменился тип dataType, то разрываем все его связи.
      newBlock.objects[index].addresses = oldBlock.objects[index].addresses;
      if (!object.active || object.dataType !== oldBlock.objects[index].dataType) {
        const promises = object.addresses.map(async addressID => {
          await dispatch('DELETE_LINK', {
            address: state.addresses.getElementByID(addressID),
            socket: {
              device,
              block: newBlock,
              object: newBlock.objects[index],
              direction: newBlock.objects[index].direction,
            },
          });
          object.addresses = [];
        });
        await Promise.all(promises);
      }
    });
    await Promise.all(globalPromises);
    commit('SET_SETTINGS_BLOCK', {
      device,
      blockID: newBlock.id,
      data: newBlock,
    });
    return 'Настройки успешно сохранены';
  },
  // Вставка скопированого элмента Tree Struct
  COPY_ELEMENT_TREE({ commit }, { struct, element, parent }) {
    if (struct.type === 'devices') {
      commit('ADD_DEVICE', { device: element, parent });
    } else {
      if (struct.type === 'addresses') element.sockets = [];
      if (struct.type === 'blocks') {
        element.objects.forEach(el => {
          el.addresses = [];
        });
      }
      struct.addElement(element, parent);
    }
  },
};

export default {
  state,
  mutations,
  actions,
};
