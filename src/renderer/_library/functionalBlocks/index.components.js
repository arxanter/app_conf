      /* Компоненты конфигурации блоков */
      /*
      AndOr: () => import('./configBlocks/AndOr'),
      Blinds: () => import('./configBlocks/Blinds'),
      Compare: () => import('./configBlocks/Compare'),
      Counter: () => import('./configBlocks/Counter'),
      Delay: () => import('./configBlocks/Delay'),
      Interval: () => import('./configBlocks/Interval'),
      ModbusRead: () => import('./configBlocks/ModbusRead'),
      ModbusWrite: () => import('./configBlocks/ModbusWrite'),
      MotionSensor: () => import('./configBlocks/MotionSensor'),
      Multiport: () => import('./configBlocks/Multiport'),
      Not: () => import('./configBlocks/Not'),
      OnOffToggle: () => import('./configBlocks/OnOffToggle'),
      Termostat: () => import('./configBlocks/Termostat'),
      OneWire: () => import('./configBlocks/OneWire'),
      Value: () => import('./configBlocks/Value'),
      Fancoil: () => import('./configBlocks/Fancoil'),
      Read: () => import('./configBlocks/Read'),
      Calendar: () => import('./configBlocks/Calendar'),
      TimeAlarm: () => import('./configBlocks/TimeAlarm'),
      Scene: () => import('./configBlocks/Scene'),
      DimmByValue: () => import('./configBlocks/DimmByValue'),
      MathBlock: () => import('./configBlocks/MathBlock'),
      ModbusRelay: () => import('./configBlocks/ModbusRelay'),
      ModbusDimmer: () => import('./configBlocks/ModbusDimmer'),
      ModbusRGB: () => import('./configBlocks/ModbusRGB'),
      ModbusRGBLightTemp: () => import('./configBlocks/ModbusRGBLightTemp'),
*/
const files = require.context('./componentsConfig', false, /\.vue|js$/);
function install(Vue) {
  files.keys().forEach((key) => {

    Vue.component(files(key).default.name, files(key).default);
  });
}

export default install;

