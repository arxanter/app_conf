

const files = {};
files.logic = require.context('./structures', false, /\.logic.js$/);
files.modbus = require.context('./structures', false, /\.modbus.js$/);
files.perform = require.context('./structures', false, /\.perform.js$/);
files.interface = require.context('./structures', false, /\.interface.js$/);
const structures = {};

Object.keys(files).forEach(nameType => {
  structures[nameType] = {};
  files[nameType].keys().forEach(key => {
    structures[nameType][files[nameType](key).fnType] = files[nameType](key);
  });
});
structures.logic.params = {
  name: {
    ru: 'Логические блоки',
    en: 'Logic blocks',
  },
};
structures.modbus.params = {
  name: {
    ru: 'Блоки Modbus',
    en: 'Modbus blocks',
  },
};
structures.perform.params = {
  name: {
    ru: 'Исполнительные блоки',
    en: 'Perform blocks',
  },
};
structures.interface.params = {
  name: {
    ru: 'Блоки интерфейсы',
    en: 'interface blocks',
  },
};
export default structures;