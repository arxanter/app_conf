module.exports = {
  id: null,
  name: 'Calendar',
  fnType: 'calendar',
  fnTypeValue: 14,
  deviceTypes: ['logicController', 'multiport'],
  settings: {
    mode: 0,
    time: [8, 30],
    weekDays: [0, 0, 0, 0, 0, 0, 0],
    monthDays: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    months: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    eventType: 1,
    eventValue: 1,
  },
  settingsTypes: {
    mode: 'uint8',
    time: 'uint8',
    weekDays: 'arrayBits',
    monthDays: 'arrayBits',
    months: 'arrayBits',
    eventType: 'uint8',
    eventValue: 'uint8',
  },
  objects: [
    {
      name: 'Block',
      addresses: [],
      dataType: 1,
      bus: 0,
      active: true,
      direction: 'input',
    },
    {
      name: 'Event',
      addresses: [],
      dataType: 1,
      bus: 0,
      active: true,
      direction: 'output',
    },
  ],
};
