module.exports = {
  id: null,
  name: 'OneWire',
  fnType: 'oneWire',
  fnTypeValue: 203,
  deviceTypes: ['logicController', 'multiport'],
  settings: {
    addressRAM: '00:00:00:00:00:00:00:00',
    deltaT: 25.5,
    deviceID: 1,
    sendCyclic: 5,
  },
  settingsTypes: {
    addressRAM: 'mac',
    deltaT: 'float',
    deviceID: 'uint8',
    sendCyclic: 'uint8',
  },
  objects: [
    {
      name: 'Temperature status',
      addresses: [],
      dataType: 6,
      bus: 0,
      active: true,
      direction: 'output',
    },
    {
      name: 'Error status',
      addresses: [],
      dataType: 2,
      bus: 0,
      active: true,
      direction: 'output',
    },
  ],
};
