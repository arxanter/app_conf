module.exports = {
  id: null,
  name: 'TimeAlarm',
  fnType: 'timeAlarm',
  fnTypeValue: 20,
  deviceTypes: ['logicController', 'multiport'],
  settings: {
    eventType: 1,
    eventValue: 0,
  },
  settingsTypes: {
    eventType: 'uint8',
    eventValue: 'uint8',
  },
  objects: [
    {
      name: 'Block',
      addresses: [],
      dataType: 1,
      bus: 0,
      active: true,
      direction: 'input',
    },
    {
      name: 'Time/day',
      addresses: [],
      dataType: 8,
      bus: 0,
      active: true,
      direction: 'input',
    },
    {
      name: 'Event',
      addresses: [],
      dataType: 1,
      bus: 0,
      active: true,
      direction: 'output',
    },
    {
      name: 'Time/day status',
      addresses: [],
      dataType: 8,
      bus: 0,
      active: true,
      direction: 'output',
    },
  ],
};
