module.exports = {
  id: null,
  name: 'On/Off/Toggle',
  fnType: 'onOffToggle',
  fnTypeValue: 3,
  deviceTypes: ['logicController', 'multiport'],
  settings: {
    controlValueReaction: 1,
    sendReadReqBeforeStart: 0,
    type: 0,
  },
  settingsTypes: {
    controlValueReaction: 'uint8',
    sendReadReqBeforeStart: 'uint8',
    type: 'uint8',
  },
  objects: [
    {
      name: 'Control',
      addresses: [],
      dataType: 1,
      bus: 0,
      active: true,
      direction: 'input',
    },
    {
      name: 'inputStatus',
      addresses: [],
      dataType: 1,
      bus: 0,
      active: false,
      direction: 'input',
    },
    {
      name: 'Output',
      addresses: [],
      dataType: 1,
      bus: 0,
      active: true,
      direction: 'output',
    },
  ],
};
