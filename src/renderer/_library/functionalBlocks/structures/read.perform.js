module.exports = {
  id: null,
  name: 'Read',
  fnType: 'read',
  fnTypeValue: 19,
  deviceTypes: ['logicController', 'multiport'],
  settings: {
    delay: 100,
    inputType: 1,
  },
  settingsTypes: {
    delay: 'uint8',
    inputType: 'uint8',
  },
  objects: [
    {
      name: 'Read object',
      addresses: [],
      dataType: 1,
      bus: 0,
      active: true,
      direction: 'input',
    },
    {
      name: 'Block',
      addresses: [],
      dataType: 1,
      bus: 0,
      active: true,
      direction: 'input',
    },
  ],
};
