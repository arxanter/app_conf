module.exports = {
  id: null,
  name: 'Logic NOT',
  fnType: 'not',
  fnTypeValue: 10,
  deviceTypes: ['logicController', 'multiport'],
  settings: {
    sendCyclic: 0,
    sendReadReqBeforeStart: 0,
    sendWhenChange: 0,
  },
  settingsTypes: {
    sendCyclic: 'uint8',
    sendReadReqBeforeStart: 'uint8',
    sendWhenChange: 'uint8',
  },
  objects: [
    {
      name: 'Input 1',
      addresses: [],
      dataType: 1,
      bus: 0,
      active: true,
      direction: 'input',
    },
    {
      name: 'Output',
      addresses: [],
      dataType: 1,
      bus: 0,
      active: true,
      direction: 'output',
    },
  ],
}