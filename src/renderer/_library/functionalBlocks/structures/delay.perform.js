module.exports = {
  id: null,
  name: 'Delay',
  fnType: 'delay',
  fnTypeValue: 12,
  deviceTypes: ['logicController', 'multiport'],
  settings: {
    delay: 100,
    inputType: 1,
  },
  settingsTypes: {
    delay: 'uint16',
    inputType: 'uint8',
  },
  objects: [
    {
      name: 'Input',
      addresses: [],
      dataType: 1,
      bus: 0,
      active: true,
      direction: 'input',
    },
    {
      name: 'Output',
      addresses: [],
      dataType: 1,
      bus: 0,
      active: true,
      direction: 'output',
    },
  ],
};
