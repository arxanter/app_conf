module.exports = {
  id: null,
  name: 'Math block',
  fnType: 'mathBlock',
  fnTypeValue: 16,
  deviceTypes: ['logicController', 'multiport'],
  settings: {
    inputType: 2,
    outputType: 2,
    expression: 'x+2',
  },
  settingsTypes: {
    inputType: 'uint8',
    outputType: 'uint8',
    expression: 'string',
  },
  objects: [
    {
      name: 'Input',
      addresses: [],
      dataType: 2,
      bus: 0,
      active: true,
      direction: 'input',
    },
    {
      name: 'Output',
      addresses: [],
      dataType: 2,
      bus: 0,
      active: true,
      direction: 'output',
    },
  ],
};
