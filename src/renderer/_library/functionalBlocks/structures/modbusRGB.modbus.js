module.exports = {
  id: null,
  name: 'modbus RGB',
  fnType: 'modbusRGB',
  fnTypeValue: 205,
  deviceTypes: ['logicController', 'multiport'],
  settings: {
    channel: 1,
    commandCode: 5,
    slaveAddress: 1,
    registerAddressChA: 1,
    registerAddressChB: 2,
    registerAddressChC: 3,
    registerAddressChD: 4,
    sendRequestIfError: 1,
    errorCycleTime: 5,
    minValue: 0,
    maxValue: 255,
    useInScene: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    scenesNumbers: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    scenesValue: [
      [77, 123, 239, 50],
      [77, 123, 239, 50],
      [77, 123, 239, 50],
      [77, 123, 239, 50],
      [77, 123, 239, 50],
      [77, 123, 239, 50],
      [77, 123, 239, 50],
      [77, 123, 239, 50],
      [77, 123, 239, 50],
      [77, 123, 239, 50],
      [77, 123, 239, 50],
      [77, 123, 239, 50],
      [77, 123, 239, 50],
      [77, 123, 239, 50],
      [77, 123, 239, 50],
      [77, 123, 239, 50],
    ],
    initStart: 0,
  },
  settingsTypes: {
    channel: 'uint8',
    commandCode: 'uint8',
    slaveAddress: 'uint8',
    registerAddressChA: 'uint16',
    registerAddressChB: 'uint16',
    registerAddressChC: 'uint16',
    registerAddressChD: 'uint16',
    sendRequestIfError: 'uint8',
    errorCycleTime: 'uint8',
    minValue: 'uint8',
    maxValue: 'uint8',
    useInScene: 'arrayBits',
    scenesNumbers: 'uint8',
    scenesValue: 'arrayUint8',
    initStart: 'uint8',
  },
  objects: [
    {
      name: 'Hue',
      addresses: [],
      dataType: 2,
      bus: 0,
      active: true,
      direction: 'input',
    },
    {
      name: 'Control ch A',
      addresses: [],
      dataType: 2,
      bus: 0,
      active: true,
      direction: 'input',
    },
    {
      name: 'Control ch B',
      addresses: [],
      dataType: 2,
      bus: 0,
      active: true,
      direction: 'input',
    },
    {
      name: 'Control ch C',
      addresses: [],
      dataType: 2,
      bus: 0,
      active: true,
      direction: 'input',
    },
    {
      name: 'Control ch D',
      addresses: [],
      dataType: 2,
      bus: 0,
      active: true,
      direction: 'input',
    },
    {
      name: 'SceneInput',
      addresses: [],
      dataType: 2,
      bus: 0,
      active: true,
      direction: 'input',
    },
    {
      name: 'SceneSave',
      addresses: [],
      dataType: 2,
      bus: 0,
      active: true,
      direction: 'input',
    },
    {
      name: 'Color',
      addresses: [],
      dataType: 7,
      bus: 0,
      active: true,
      direction: 'input',
    },
    {
      name: 'Hue dimm',
      addresses: [],
      dataType: 4,
      bus: 0,
      active: true,
      direction: 'input',
    },
    {
      name: 'On/Off',
      addresses: [],
      dataType: 1,
      bus: 0,
      active: true,
      direction: 'input',
    },
    {
      name: 'Status ch A',
      addresses: [],
      dataType: 2,
      bus: 0,
      active: true,
      direction: 'output',
    },
    {
      name: 'Status ch B',
      addresses: [],
      dataType: 2,
      bus: 0,
      active: true,
      direction: 'output',
    },
    {
      name: 'Status ch C',
      addresses: [],
      dataType: 2,
      bus: 0,
      active: true,
      direction: 'output',
    },
    {
      name: 'Status ch D',
      addresses: [],
      dataType: 2,
      bus: 0,
      active: true,
      direction: 'output',
    },
    {
      name: 'Error',
      addresses: [],
      dataType: 2,
      bus: 0,
      active: true,
      direction: 'output',
    },
    {
      name: 'Status color',
      addresses: [],
      dataType: 7,
      bus: 0,
      active: true,
      direction: 'output',
    },
    {
      name: 'Status on/off',
      addresses: [],
      dataType: 1,
      bus: 0,
      active: true,
      direction: 'output',
    },
  ],
};
