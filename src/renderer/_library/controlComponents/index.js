import Wrapper from './Wrapper';
import ZhSwitch from './Zh-switch';
import ZhCheckbox from './Zh-checkbox';
import ZhCheckboxArray from './Zh-checkbox-array';
import ZhCheckboxTableMode from './Zh-checkbox-table-mode';
import ZhInputNumber from './Zh-input-number';
import ZhInputRange from './Zh-input-range';
import ZhSelect from './Zh-select';
import ZhMultiSelect from './Zh-multi-select';
import ZhInput from './Zh-input';
import ZhRadio from './Zh-radio';
import ZhSlider from './Zh-slider';
import ZhRange from './Zh-range';
import ZhTimePicker from './Zh-time-picker';
import ZhClockPicker from './Zh-clock-picker';
import ZhColorPicker from './Zh-color-picker';
import ZhFanSlider from './Zh-fan-slider';
import ZhButtonIcon from './Zh-button-icon';
import ZhWrapper from './Zh-wrapper';
import ZhInputKnx from './clear/Zh-input-knx';
import ZhInputAddressKnx from './clear/Zh-input-address-knx';

const components = [
  ZhSwitch,
  ZhCheckbox,
  ZhCheckboxArray,
  ZhCheckboxTableMode,
  ZhInputNumber,
  ZhInputRange,
  ZhSelect,
  ZhMultiSelect,
  ZhInput,
  ZhRadio,
  ZhSlider,
  ZhRange,
  ZhTimePicker,
  ZhClockPicker,
  ZhColorPicker,
  ZhFanSlider,
  ZhButtonIcon,
  ZhWrapper,
  ZhInputKnx,
  ZhInputAddressKnx
  
];

const install = (Vue) => {
  components.forEach(component => {
    Vue.component(component.name, component);
  })
  Vue.component('Wrapper', Wrapper);
}

export default install;