const req = require.context('./structures', false, /\.js$/);
const modules = [];
req.keys().forEach((key) => {
  if (key === './index.js') return;
  modules.push(req(key));
});

export default modules;