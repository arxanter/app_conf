const files = require.context('./components', false, /\.vue|js$/);
function install(Vue) {
  files.keys().forEach((key) => {

    Vue.component(files(key).default.name, files(key).default);
  });
}

export default install;