import { checkCRC8 } from '../service/workCRC';
const net = require('net');

/**
 * Function try connect to device
 * @function connector
 * @param {String} ip 
 * @param {Number} port 
 * @param {<EventBus>} eventBus 
 * @return {Promise} socket
 * 
 */
const connector = (ip, port, eventBus) =>
  new Promise((resolve, reject) => {
    if (!ip || !port) throw new Error('Uncorrect params port or ip');
    const socket = net.Socket();
    socket.setTimeout(2000);
    socket.on('error', err => {
      console.log(err);
      eventBus.emit('configTCP:response', {
        ok: false,
        text: 'Problem with connection',
        err,
        socket,
      });
      reject(socket);
    });
    socket.on('timeout', () => {
      eventBus.emit('configTCP:response', {
        ok: false,
        text: 'Problem with connection',
        err: new Error('Timeout connection'),
        socket,
      });
      reject(socket);
    });
    // Start connection
    eventBus.emit('configTCP:response', { ok: true, text: 'Start connecting to device' });
    socket.connect(
      port,
      ip,
      () => {
        eventBus.emit('configTCP:response', { ok: true, stage: 1, text: 'Connect to Device' });
        resolve(socket);
      }
    );
  });
/**
 * Function send data to device
 * @function sender
 * @param {<SocketObject>} socket Сокет по которому отправлять данные
 * @param {Object} data Данные для отправки
 * @return {Promise} responseData || err
 */
const sender = (socket, data) =>
  new Promise((resolve, reject) => {
    if (data) {
      let timer = null;
      socket.once('data', data => {
        console.log('Данные ответа');
        console.log(data);
        console.log(`Длина ${data.length} byte`)
        clearTimeout(timer);
        // Check crc response
        const response = checkCRC8(data);
        if (!response) reject(new Error('Wrong response'));
        resolve(data);
      });
      console.log('Данные отправки');
      console.log(data);
      console.log(`Длина ${data.length} byte`)
      socket.write(data, () => {
        timer = setTimeout(() => {
          reject(new Error('No response'));
          socket.end();
        }, 2000);
      });
    }
  });

export { connector, sender };
