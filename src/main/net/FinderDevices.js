import { checkCRC8 } from '../service/workCRC';
import MulticastSocket from './multicastSocket';
const EventEmitter = require('events');
const eventBUS = new EventEmitter();

/**
 * Класс поиска устройств.
 * @class FinderDevices
 * @property {<eventEmitter>} eventEmitter Шина для связи с остальным main процессом
 * @property {<eventBUS>} eventBUS Шина для связи с IPC рендер и main процессом
 * @property {String} ip
 * @property {Number} port
 * @property {Socket} socket Сокет общения
 *
 * @method start Запуск периодического опроса устройств
 * @method off  Остановка опроса устройств
 * @method send  Отправка данных по сокету опроса
 * @method errorHandler  Перехватчик ошибок опроса
 * @method msgHandler  Перехватчик сообщений по сокету опроса
 * @method readyHandler  Перехватчик готовности сокета к опросу и обменом сообщениями
 * 
 */
export default class FinderDevices {
  constructor(eventEmitter, ip, port) {
    this.data = null;

    this.eventEmitter = eventEmitter;
    this.eventBUS = eventBUS;
    this.multicastIP = ip;
    this.multicastPORT = port;
    this.socket = new MulticastSocket(eventBUS, ip, port);
    this.eventBUS.on('multicast:error', err => {
      this.errorHandler(err);
    });
    this.eventBUS.on('multicast:message', data => {
      this.msgHandler(data);
    });
    this.eventBUS.on('multicast:ready', () => {
      this.readyHandler();
    });
  }
  start() {
    if (this.socket.ready) this.socket.off();
    this.socket.start();
  }
  off() {
    if (this.socket.ready === true) this.socket.off();
  }
  send(data) {
    console.log('Запрос устройств');
    // console.log(`Socket ready: ${this.socket.ready}`);
    if (!this.socket.ready) this.socket.start();
    if (this.socket.ready) {
      console.log('Отправка запроса. Тело запроса:', data);
      this.socket
        .send(data)
        .then(() => console.log(''))
        .catch(err => this.errorHandler(err));
    }
  }

  errorHandler() {
    console.log('Отправка не удалась FinderDevices');
    this.eventEmitter.emit('finderDevices:error');
    this.socket.off();
  }
  msgHandler(data) {
    try {
      const isCorrect = checkCRC8(data);
      const msgCode = data.readInt16BE();
      const msgLength = data.readInt16BE(2);
      if (msgCode === 22 && msgLength === 14 && isCorrect) {
        const response = {
          mac: '',
          ip: '',
          fnType: null,
        };
        for (let i = 0; i < 6; i++) {
          if (i !== 0) response.mac += ':';
          const newPart = data
            .readUInt8(4 + i)
            .toString(16)
            .toUpperCase();
          response.mac += newPart.length < 2 ? `0${newPart}` : newPart;
        }
        for (let i = 0; i < 4; i++) {
          if (i !== 0) response.ip += '.';
          response.ip += data.readUInt8(10 + i);
        }
        response.fnTypeCode = data.readInt16BE(16);
        this.eventEmitter.emit('finderDevices:findedDevice', response);
      }
    } catch (err) {
      console.log('Некорректное Multicast собщение');
    }
  }
  // eslint-disable-next-line
  readyHandler() {
    console.log('Сокет опроса устройств успешно открыт');
  }
}
