/**
 *
 * Sender and receiver of multicast
 *
 */
import MulticastSocket from './multicastSocket';
import { calcCRC8, checkCRC8 } from '../service/workCRC';
const EventEmitter = require('events');
const eventBUS = new EventEmitter();

// Service funciton

/**
 * @function giveTypeTelegram
 * @param {Number} value Байт типа телеграммы
 * @return { String} Тип телеграммы
 */

const giveTypeTelegram = value => {
  switch (
    value >> 3 // eslint-disable-line no-bitwise
  ) {
    case 0b00001:
      return 'Запрос значения';
    case 0b00010:
      return 'Ответ на запрос значения';
    case 0b00011:
      return 'Запись значения';
    case 0b00100:
      return 'Запрос значения физического адреса';
    case 0b00101:
      return 'Ответ на запрос значения физического адреса';
    case 0b00110:
      return 'Запись значения физического адреса';
    case 0b00111:
      return 'Запрос файла конфигурации';
    case 0b01000:
      return 'Ответ на запрос файла конфигурации';
    case 0b01001:
      return 'Запись файла конфигурации';
    case 0b01010:
      return 'Запрос маски конфигурации';
    case 0b01011:
      return 'Ответ на запрос маски конфигурации';
    case 0b01100:
      return 'Сброс устройства';
    default:
      return 'UNKNOWN TYPE';
  }
};
/**
 *
 * !!!! Написать обработку всех случаев некорректных данных.
 *
 */
/**
 *  Функция парсинга данных телеграммы
 * @function giveDataTelegram
 * @param {BufferArray} buf Буфер полученных данных
 * @return {Number | String} Команда телеграммы
 */
const giveDataTelegram = buf => {
  const dataType = buf.readUInt8(7);
  switch (dataType) {
    /* Тип 1 (Дискретное значение) 1 byte */
    case 1:
      return buf.readUInt8(8);
    /* Тип 2 (Беззнаковое число)  1 byte */
    case 2:
      return buf.readUInt8(8);
    /* Тип 3 ( Знаковое число) 1 byte */
    case 3:
      return buf.readInt8(8);
    /* Тип 4 ( Управление диммером) 1 byte */
    case 4:
      if (buf.readUInt8(8) === 0xf0 || buf.readUInt8(8) === 0x00) return 'STOP';
      if (buf.readUInt8(8) === 0xff) return 'UP';
      if (buf.readUInt8(8) === 0x0f) return 'DOWN';
      return null;
    /* Тип 5 ( Беззнаковое число) 2 byte */
    case 5:
      return buf.readUInt16BE(8);
    /* Тип 6 ( Знаковое число с запятой) 4 byte */
    case 6:
      return buf.readFloatBE(8).toFixed(2);
    /* Тип 7 ( RGB color) 3 byte */
    case 7:
      // Записываем данные
      return [...buf.slice(8, -1)];
    /* Тип 8 ( Время и день недели) 4 byte */
    case 8:
      return [...buf.slice(9, -1)];
    /* Тип 9 ( Дата) 4 byte */
    case 9:
      return [buf.readUInt8(8), buf.readUInt8(9), buf.readUInt16BE(10)];
    /* Тип 10 ( Беззнаковое число) 4 byte */
    case 10:
      return buf.readUInt32BE(8);
    /* Тип 11 ( Знаковое число) 4 byte */
    case 11:
      return buf.readInt32BE(8);
    default:
      return 'ERROR DATA';
  }
};
/**
 * Функция парсинга телеграммы
 * @function parsingTelegram
 * @param {ArrayBuffer} buf телеграмма
 * @return {<telegramObject{source, target, type, dataType}>} 
 */
const parsingTelegram = buf => {
  const isValid = checkCRC8(buf);
  console.log('^^^^^^^^^^^^^');
  console.log(buf);
  console.log('^^^^^^^^^^^^^');
  if (isValid) {
    const data = {};
    data.source = buf.readUInt16BE(1);
    data.target = buf.readUInt16BE(3);
    data.type = giveTypeTelegram(buf.readUInt8(6));
    data.dataType = buf.readUInt8(7);
    if (data.type !== 'Запрос значения') data.data = giveDataTelegram(buf);
    else data.data = '';
    const time = new Date();
    data.time = time.toLocaleString('ru');
    return data;
  }
  console.log('ERROR SRC CHECK');
  return null;
};

/**
 * Класс Multicast.
 * @class 
 * @property {<eventEmitter>} eventGlobal Шина для связи с остальным main процессом
 * @property {<eventBUS>} eventBUS Шина для связи с IPC рендер и main процессом
 * @property {String} multicastIP
 * @property {Number} multicastPORT
 * @property {Socket} socket Сокет общения
 *
 * @method start Запуск периодического опроса устройств
 * @method off  Остановка опроса устройств
 * @method send  Отправка данных по сокету опроса
 * @method errorHandler  Перехватчик ошибок опроса
 * @method msgHandler  Перехватчик сообщений по сокету опроса
 * @method readyHandler  Перехватчик готовности сокета
 *
 */
export default class Multicast {
  constructor(eventEmitter, ip, port) {
    this.data = null;
    this.eventGlobal = eventEmitter;
    this.eventBUS = eventBUS;
    this.multicastIP = ip;
    this.multicastPORT = port;
    this.socket = new MulticastSocket(eventBUS, ip, port);
    this.eventBUS.on('multicast:error', () => {
      this.errorHandler();
    });
    this.eventBUS.on('multicast:message', data => {
      this.msgHandler(data);
    });
    this.eventBUS.on('multicast:ready', () => {
      this.readyHandler();
    });
  }

  start() {
    if (this.socket.ready) this.socket.off();
    this.socket.start();
  }

  off() {
    if (this.socket.ready) this.socket.off();
  }

  // Send msg to multicast channel
  async send(msg) {
    if (!this.socket.ready) this.socket.start();
    if (this.socket.ready) {
      // Буфер описания пакета и буфер данных
      const bufInit = Buffer.alloc(7);
      let bufData = null;
      //
      bufInit.writeUInt8(0b10111111);
      // Временный адрес интерфейса FFFF
      bufInit.writeUInt16BE(0xffff, 1);
      bufInit.writeUInt16BE(+msg.target, 3);
      // Write 6 byte bufInit
      bufInit.writeUInt8(0b11000000 + msg.dataLength + 1, 5);
      // if msg req value
      if (msg.isReq) {
        // Write 7 byte
        bufInit.writeUInt8(0b00001111, 6);
        bufData = Buffer.from([msg.dataType]);
      } else {
        // Write 7 byte
        bufInit.writeUInt8(0b00011111, 6);
        bufData = Buffer.alloc(msg.dataLength + 1);
        bufData.writeUInt8(msg.dataType);
        switch (msg.dataType) {
          case 1:
            bufData.writeUInt8(msg.data, 1);
            break;
          case 2:
            bufData.writeUInt8(msg.data, 1);
            break;
          case 3:
            bufData.writeInt8(msg.data, 1);
            break;
          case 4:
            bufData.writeUInt8(msg.data, 1);
            break;
          case 5:
            bufData.writeUInt16BE(msg.data, 1);
            break;
          case 6:
            bufData.writeFloatBE(msg.data, 1);
            break;
          case 7:
            // Записываем данные
            msg.data.forEach((el, index) => {
              bufData.writeUInt8(el, index + 1);
            });
            break;
          case 8:
            // Первый байт не используем
            bufData.writeUInt8(0, 1);
            // Записываем данные
            msg.data.forEach((el, index) => {
              bufData.writeUInt8(el, index + 2);
            });
            break;
          case 9:
            bufData.writeUInt8(msg.data[0], 1);
            bufData.writeUInt8(msg.data[1], 2);
            bufData.writeUInt16BE(msg.data[2], 3);
            break;
          case 10:
            bufData.writeUInt32BE(msg.data, 1);
            break;
          case 11:
            bufData.writeInt32BE(msg.data, 1);
            break;
          default:
            console.log('ERROR DATA TYPE');
            break;
        }
      }
      const dataSend = Buffer.concat([bufInit, bufData], bufInit.length + bufData.length + 1);
      calcCRC8(dataSend, true);
      await this.socket.send(dataSend);
    } else this.errorHandler();
  }

  errorHandler() {
    this.eventGlobal.emit('multicastBUS:error');
    if (this.socket.ready) this.socket.off();
  }

  //
  msgHandler(message) {
    try {
      console.log(message);
      const data = parsingTelegram(message);
      this.eventGlobal.emit('multicastBUS:message', data);
    } catch (err) {
      console.log('ERROR parse multicast msg');
    }
  }
  readyHandler() {
    this.eventGlobal.emit('multicastBUS:ready');
  }
}
