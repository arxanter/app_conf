/* eslint no-bitwise: "off" */
import { calcCRC8 } from './workCRC';

const packForBus = (data, typeDatagram) => {
  const buffer = Buffer.from([0x00, 0x00, 0x00, 0x00, ...data, 0x00]);
  switch (typeDatagram) {
    case 'block':
      buffer.writeUInt16BE(9, 0);
      break;
    case 'blockEnd':
      buffer.writeUInt16BE(11, 0);
      break;
    case 'visio':
      buffer.writeUInt16BE(25, 0);
      break;
    case 'settingsDevice':
      buffer.writeUInt16BE(1, 0);
      break;
    case 'reqSettings':
      buffer.writeUInt16BE(3, 0);
      break;
    case 'configModbus':
      buffer.writeUInt16BE(5, 0);
      break;
    case 'reqModbus':
      buffer.writeUInt16BE(7, 0);
      break;
    case 'sendMsgModbus':
      buffer.writeUInt16BE(17, 0);
      break;
    case 'reqOneWire':
      buffer.writeUInt16BE(13, 0);
      break;
    case 'reqValueOneWire':
      buffer.writeUInt16BE(15, 0);
      break;
    case 'sendTabkeKNX':
      buffer.writeUInt16BE(19, 0);
      break;
    case 'findDevice':
      buffer.writeUInt16BE(21, 0);
      break;
    case 'resetDevice':
      buffer.writeUInt16BE(23, 0);
      break;
    default:
      throw new Error('Uncorrect datagram Type');
  }
  buffer.writeUInt16BE(data.length, 2);
  calcCRC8(buffer, true);
  return buffer;
};

/**
 *
 * pack func. block
 */

const packBlocks = block => {
  const maxBufferLength = 1000;
  const buffer = Buffer.alloc(maxBufferLength);
  let countByte = 0;
  function writeData(param, dataType) {
    switch (dataType) {
      case 'uint8':
        buffer.writeUInt8(param, countByte);
        countByte += 1;
        break;
      case 'uint16':
        buffer.writeUInt16BE(param, countByte);
        countByte += 2;
        break;
      case 'float':
        buffer.writeFloatBE(param, countByte);
        countByte += 4;
        break;
      case 'string':
        buffer.write(param, countByte);
        // Условие, что длина строки 50 байт
        countByte += 50;
        break;
      case 'arrayBits':
        do {
          if (param.length % 8 !== 0) param.push(0);
        } while (param.length % 8 !== 0);
        param.reverse();
        for (let i = 0; i < Math.round(param.length / 8); i++) {
          const value = parseInt(param.slice(i * 8, (i + 1) * 8).join(''), 2);
          writeData(value, 'uint8');
        }
        break;
      case 'arrayUint8':
        param.forEach(el => {
          writeData(el, 'uint8');
        });
        break;
      case 'arrayUint16':
        param.forEach(el => {
          writeData(el, 'uint16');
        });
        break;
      case 'mac':
        param.split(':').forEach(el => {
          writeData(parseInt(el, 16), 'uint8');
        });
        break;
      default:
        throw new Error(`Unknow type value: ${dataType}`);
    }
  }
  /**
   * pack ID block
   */
  buffer.writeUInt8(+block.id, countByte);
  countByte += 1;
  /**
   * pack logicType block
   */
  buffer.writeUInt8(block.fnTypeValue, countByte);
  countByte += 1;
  /**
   * pack hashUID
   */
  buffer.writeUInt32BE(parseInt(block.hashUID, 16), countByte);
  countByte += 4;

  /**
   * pack clearVariables parameter
   */
  buffer.writeInt8(block.clearVariables, countByte);
  countByte += 1;
  /**
   * pack settings block
   */
  if (block.settings) {
    const arraySettings = Object.keys(block.settings);
    // Alphabetic Sort array settings block
    arraySettings.sort((a, b) => (a > b ? 1 : -1));
    arraySettings.forEach(key => {
      const type = block.settingsTypes[key];
      const value = block.settings[key];
      // Check if param is Array of value. And check if that value type - arrayType. Then write value to buffer.
      if (type === undefined) {
        console.log(`Параметр ${key} не отправляется`);
      } else if (
        typeof value === 'number' ||
        typeof value === 'string' ||
        (type.includes('array') && typeof value[0] === 'number')
      ) {
        writeData(value, type);
      } else if (Array.isArray(value) || (type.includes('array') && Array.isArray(value[0]))) {
        value.forEach(el => {
          writeData(el, type);
        });
      } else throw new Error('Uncorrect setting value');
    });
  }
  /**
   *
   * Pack objects of block
   */
  block.objects.forEach(object => {
    // Write bus object
    buffer.writeUInt8(object.bus, countByte);
    countByte += 1;
    // Write dataType object
    buffer.writeUInt8(object.dataType, countByte);
    countByte += 1;
    // Write direction object
    buffer.writeUInt8(+(object.direction === 'output'), countByte);
    countByte += 1;
    // Write addresses object
    // If ibject type 'input'. We pack 14 address. If the address is empty, fill with zero
    if (object.direction === 'input') {
      object.addresses.forEach(el => {
        buffer.writeUInt16BE(el, countByte);
        countByte += 2;
      });
      // If addresses less than 14, wrote 0 at empty positions
      if (object.addresses.length < 14) {
        for (let i = 0; i < 14 - object.addresses.length; i++) {
          buffer.writeUInt16BE(0, countByte);
          countByte += 2;
        }
      }
    } else if (object.direction === 'output') {
      // If object type 'output'. We pack only 1 address.
      if (object.addresses[0]) buffer.writeUInt16BE(object.addresses[0], countByte);
      else buffer.writeUInt16BE(0, countByte);
      countByte += 2;
    }
  });

  const dataSend = buffer.slice(0, countByte);
  return packForBus(dataSend, 'block');
};
/**
 *
 * pack Net config
 */
const packSettingsDevice = ({ settings, deviceID, modbus, deviceType }) => {
  const bufferLength = 200;
  const buffer = Buffer.alloc(bufferLength);
  let countByte = 0;

  // Function which concatenates  string value IP address and wrote in buffer
  const writeIP = (data) => {
    const arrayIP = typeof data === 'string' ? data.split('.') : [255,255,255,255];
    if (arrayIP.length < 4) throw Error('Uncorrect ip');
    arrayIP.forEach(el => {
      buffer.writeUInt8(+el, countByte);
      countByte += 1;
    });
  };
  // Function which wrote number of port  in buffer
  const writePort = data => {
    buffer.writeUInt16BE(data, countByte);
    countByte += 2;
  };
  buffer.writeUInt16BE(deviceType);
  countByte += 2;
  // Режим DHCP или Static
  buffer.writeUInt8(settings.isDHCP, countByte);
  countByte += 1;
  // IP адрес
  writeIP(settings.ip);
  // Маска сети
  writeIP(settings.mask);
  // Шлюз сети
  writeIP(settings.gateway);
  // IP адрес мультикаста шины
  writeIP(settings.ipMulticast);
  // IP адрес NTP сервера
  writeIP(settings.ipNTP || 0);
  // Статус использования KNX
  buffer.writeUInt8(settings.isKNX || 0, countByte);
  countByte += 1;
  // Режим работы KNX
  buffer.writeUInt8(settings.modeKNX || 0, countByte);
  countByte += 1;
      // Физический адрес KNX устройства
  if (settings.addressKNX) {
    buffer.writeUInt8(settings.addressKNX[0], countByte);
    const addressKNX = (buffer.readUInt8(countByte) << 4) + settings.addressKNX[1];
    buffer.writeUInt8(addressKNX, countByte);
    countByte += 1;
    buffer.writeUInt8(settings.addressKNX[2], countByte);
    countByte += 1;
  } else countByte += 2;

  // IP адрес KNX Multicast
  writeIP(settings.multicastKNX);
  // IP адрес KNX Tunnel
  writeIP(settings.tunnelKNX);
  // Порт мультикаст шины
  writePort(settings.portMulticast);
  // Порт конфигурирования TCP
  writePort(settings.portConfigTCP);
  // Физический адрес устройства
  buffer.writeUInt16BE(deviceID, countByte);
  countByte += 2;
  // Сохранять переменные FR
  buffer.writeUInt8(settings.variableSave, countByte);
  countByte += 1;
  // Modbus настройки.
  if (modbus) {
    modbus.forEach(channel => {
      buffer.writeUInt16BE(channel.speed / 100, countByte);
      countByte += 2;
      buffer.writeUInt8(channel.parity, countByte);
      countByte += 1;
      buffer.writeUInt8(channel.stopBits, countByte);
      countByte += 1;
    });
  } else countByte += 4;
  console.log(buffer.slice(0, countByte));
  return packForBus(buffer.slice(0, countByte), 'settingsDevice');
};

/**
 *
 * pack for Modbus
 */

const packModbus = ({ settings, deviceType }) => {
  const bufferLength = 100;
  const buffer = Buffer.alloc(bufferLength);
  let countByte = 0;
  buffer.writeUInt16BE(deviceType);
  countByte += 2;
  settings.forEach(channel => {
    buffer.writeUInt16BE(channel.speed / 100, countByte);
    countByte += 2;
    buffer.writeUInt8(channel.parity, countByte);
    countByte += 1;
    buffer.writeUInt8(channel.stopBits, countByte);
    countByte += 1;
  });
  return packForBus(buffer.slice(0, countByte), 'configModbus');
};
/**
 *
 * pack for Modbus Telegram
 *  telegram: { channel,  address, register, type, value, },
 */

const packModbusTelegram = telegram => {
  const bufferLength = 7;
  const buffer = Buffer.alloc(bufferLength);
  let countByte = 0;
  buffer.writeUInt8(telegram.channel, countByte);
  countByte += 1;
  buffer.writeUInt8(telegram.address, countByte);
  countByte += 1;
  buffer.writeUInt16BE(telegram.register, countByte);
  countByte += 2;
  buffer.writeUInt8(telegram.type, countByte);
  countByte += 1;
  buffer.writeUInt16BE(telegram.value, countByte);
  countByte += 2;
  return packForBus(buffer, 'sendMsgModbus');
};

/**
 *
 * pack for table KNX
 *  data: [{addressID, addressKNX: [Number,Number,Number]}, dataType}, ...]
 */
const packTableKNX = table => {
  const bufferLength = table.length * 5;
  const buffer = Buffer.alloc(bufferLength);
  let countByte = 0;
  table.forEach(item => {
    buffer.writeUInt16BE(item.addressID, countByte);
    countByte += 2;
    const addressKNX = item.addressKNX[0] * 2048 + item.addressKNX[1] * 256 + item.addressKNX[2];
    buffer.writeUInt16BE(addressKNX, countByte);
    countByte += 2;
    buffer.writeUInt8(item.dataType, countByte);
    countByte += 1;
  });
  return packForBus(buffer, 'sendTabkeKNX');
};

const packPollingDevices = data => {
  /**
   * Пока нет сборщика данных.
   */
  const bufferLength = 20;
  const buffer = Buffer.alloc(bufferLength);
  let countByte = 0;
  buffer.writeUInt16BE(data.addressID, countByte);
  countByte += 2;
  return buffer;
};
const packFindDevice = (data, type) => {
  // Если приходит Mac адрес. Отправляем его. Иначе заполняем FF:FF:FF:FF:FF:FF
  const buf = Buffer.alloc(6);
  const arrayMac = data ? data.split(':').map(el => parseInt(el, 16)) : [255, 255, 255, 255, 255, 255];
  for (let i = 0; i < arrayMac.length; i++) {
    buf.writeUInt8(arrayMac[i], i);
  }
  return packForBus(buf, type);
};
const packResetDevice = (data, type) => {
  // Если приходит Mac адрес. Отправляем его. Иначе заполняем FF:FF:FF:FF:FF:FF
  const buf = Buffer.alloc(6);
  const arrayMac = data ? data.split(':').map(el => parseInt(el, 16)) : [255, 255, 255, 255, 255, 255];
  for (let i = 0; i < arrayMac.length; i++) {
    buf.writeUInt8(arrayMac[i], i);
  }
  return packForBus(buf, type);
};
/**
 *
 * Parser
 */

const parserSettingsDevice = data => {
  try {
    const settingsValue = {
      isDHCP: data.readUInt8(),
      ip: data.slice(1, 5).join('.'),
      mask: data.slice(5, 9).join('.'),
      gateway: data.slice(9, 13).join('.'),
      ipMulticast: data.slice(13, 17).join('.'),
      ipNTP: data.slice(17, 21).join('.'),
      isKNX: data.readUInt8(21),
      modeKNX: data.readUInt8(22),
      multicastKNX: data.slice(25, 29).join('.'),
      tunnelKNX: data.slice(29, 33).join('.'),
      portMulticast: data.readUInt16BE(33),
      portConfigTCP: data.readUInt16BE(35),
      variableSave: data.readUInt8(39),
    };
    // Читаем значение KNX адрес
    settingsValue.addressKNX = [];
    const firstByte = data.readUInt8(23);
    settingsValue.addressKNX.push((firstByte & 0xf0) >> 4);
    settingsValue.addressKNX.push(firstByte & 0x0f);
    settingsValue.addressKNX.push(data.readUInt8(24));
    return settingsValue;
  } catch (err) {
    console.log(err);
    throw Error('Некорректный пакет данных');
  }
};
const parserSettingsModbus = data => {
  try {
    const countChannels = Math.floor(data.length / 4);
    if (countChannels !== data.length / 4) throw Error('Ошибка пакета данных');
    const modbus = [];
    for (let i = 0; i < countChannels; i++) {
      modbus.push({
        speed: data.readUInt16BE(i * 4),
        parity: data.readUInt8(i * 4 + 2),
        stopBits: data.readUInt8(i * 4 + 3),
      });
    }
    return modbus;
  } catch (err) {
    console.log('Modbus');
    console.log(err);
    throw Error('Некорректный пакет данных');
  }
};

/**
 *
 * Main function
 */

const packData = (data, type) => {
  switch (type) {
    case 'block':
      return packBlocks(data);
    case 'visio':
      return packForBus(data, type);
    case 'settingsDevice':
      return packSettingsDevice(data);
    case 'configModbus':
      return packModbus(data);
    case 'sendMsgModbus':
      return packModbusTelegram(data);
    case 'blockEnd':
      return packForBus(Buffer.from([data]), type);
    case 'reqSettings':
      return packForBus(Buffer.from([]), type);
    case 'reqModbus':
      return packForBus(Buffer.from([]), type);
    case 'reqOneWire':
      return packForBus(Buffer.from([]), type);
    case 'reqValueOneWire':
      return packForBus(Buffer.from(data), type);
    case 'sendTabkeKNX':
      return packTableKNX(data);
    case 'findDevice':
      return packFindDevice(data, type);
    case 'resetDevice':
      return packResetDevice(data, type);
    default:
      throw Error('Wrong type data');
  }
};
const parserData = (data, type) => {
  switch (type) {
    case 'settingsDevice':
      return parserSettingsDevice(data);
    case 'settingsModbus':
      return parserSettingsModbus(data);
    default:
      throw Error('Wrong type data');
  }
};

export { packData, parserData, packSettingsDevice, packBlocks, packForBus, packPollingDevices };
