/* eslint-disable no-bitwise */
const calcCRC8 = (buffer, isSetCRC = false) => {
  const data = isSetCRC ? buffer.slice(0, -1) : buffer;
  let crc = 0;
  for (let i = 0; i < data.length; i++) {
    let byte = data[i];
    for (let i = 0; i < 8; i++) {
      // счетчик битов в байте
      let fb = crc ^ byte;
      fb &= 1;
      crc >>= 1;
      byte >>= 1;
      if (fb === 1) crc ^= 0x8c; // полином
    }
  }
  if (isSetCRC) buffer.writeUInt8(crc, buffer.length - 1);
  return crc;
};
const checkCRC8 = data => {
  if (data) {
    const dataCRC = data.readUInt8(data.length - 1);
    const crc = calcCRC8(data.slice(0, -1));
    return dataCRC === crc;
  }
  return false;
};
const calcCRC32 = data => {
  if (typeof data !== 'string') data = data.join('');
  const polinom = 0xedb88320;
  const createTableCRC = (polinom) => {
    let crc = null;
    const table = [];
    for (let n = 0; n < 256; n++) {
      crc = n;
      for (let k = 0; k < 8; k++) {
        crc = crc & 1 ? polinom ^ (crc >>> 1) : crc >>> 1;
      }
      table[n] = crc;
    }
    return table;
  };
  const crcTable = createTableCRC(polinom);
  let myCRC = 0xffffffff;
  for (let n = 0; n < data.length; ++n)
    myCRC = (myCRC >>> 8) ^ crcTable[(myCRC ^ data.charCodeAt(n)) & 0xFF];
  myCRC = (myCRC ^ 0xffffffff) >>> 0
  return myCRC.toString(16);
};
export { calcCRC8, checkCRC8, calcCRC32 };
// module.exports = { calcCRC8, checkCRC8 };
