import { app, BrowserWindow, Menu } from 'electron'; // eslint-disable-line
import { ipcWorkerStart, ipcWorkerEnd } from './other/ipcWorker';

const EventEmitter = require('events');
const eventEmitter = new EventEmitter();
/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */

/**
 * Временное решение
 */

/* eslint-disable */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path')
    .join(__dirname, '/static')
    .replace(/\\/g, '\\\\');
}
/* eslint-enable */
let mainWindow;
const winURL =
  process.env.NODE_ENV === 'development' ? 'http://localhost:9080/index.html' : `file://${__dirname}/index.html`;

function createWindow() {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 600,
    useContentSize: true,
    width: 1000,
    minHeight: 600,
    minWidth: 1000,
    webPreferences: {
      nodeIntegration: true,
    },
  });
  const template = [
    {
      label: 'FromScratch',
      submenu: [
        {
          label: 'Quit',
          accelerator: 'CmdOrCtrl+Q',
          click: () => {
            app.quit();
          },
        },
        {
          label: 'Toggle Full Screen',
          accelerator: 'CmdOrCtrl+F',
          click: () => {
            mainWindow.setFullScreen(!mainWindow.isFullScreen());
          },
        },
        { role: 'toggledevtools' },
      ],
    },
    {
      label: 'Edit',
      submenu: [
        { role: 'undo' },
        { role: 'redo' },
        { type: 'separator' },
        { role: 'copy' },
        { role: 'paste'},
        {
          label: 'Copy Element',
          accelerator: 'Shift+C',
          click: () => {
            mainWindow.webContents.send('keyEvent:copy');
          },
        },
        {
          label: 'Paste Element',
          accelerator: 'Shift+V',
          click: () => {
            mainWindow.webContents.send('keyEvent:paste');
          },
        },
      ],
    },
  ];
  const menu = Menu.buildFromTemplate(template);
  Menu.setApplicationMenu(menu);

  mainWindow.loadURL(winURL);

  mainWindow.on('closed', () => {
    console.log('Событие closed');
    ipcWorkerEnd(eventEmitter);
    mainWindow = null;
  });
}
// For Rasberry Pi
// app.disableHardwareAcceleration()
app.on('ready', () => {
  createWindow();
  /*
  console.log = (...data) => {
    try {
      data.forEach(el => {
        mainWindow.webContents.send('logFromMain', el);
      });
    } catch (err) {
      console.error('Ошибка окна webContents');
    }
    return undefined;
  };
  */
  ipcWorkerStart(mainWindow.webContents, eventEmitter);
});

app.on('window-all-closed', () => {
  ipcWorkerEnd(eventEmitter);
  app.quit();
});

app.on('activate', () => {
  console.log('Событие activate');
  if (mainWindow === null) {
    createWindow();
  }
  // ipcWorkerEnd(eventEmitter);
  //  ipcWorkerStart(mainWindow.webContents, eventEmitter);
});
