const homeDir = `${require('os').homedir()}/.appConf`;
const desktopPath = require('path').join(require('os').homedir(), 'Desktop');
const fs = require('fs');
const path = require('path');
const util = require('util');
const readdir = util.promisify(fs.readdir);
const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);
const unlink = util.promisify(fs.unlink);
const rmdir = util.promisify(fs.rmdir);
const stat = util.promisify(fs.stat);
export default function() {
  // test folder or file if miss - creat
  const testDir = (path, file = false) =>
    new Promise((resolve, reject) => {
      try {
        fs.stat(path, err => {
          if (err) {
            if (err.code === 'ENOENT') {
              if (file)
                fs.writeFile(path, '', err => {
                  if (err) throw err;
                  else resolve();
                });
              else
                fs.mkdir(path, err => {
                  if (err) throw err;
                  else resolve();
                });
            } else {
              throw Error('Problem with open path');
            }
          } else resolve();
        });
      } catch (err) {
        reject(err);
      }
    });
  /**
   * test init files and if miss - creat, or return value
   * @function openInit
   * @return {Promise<Object | Error>}  initData files | Error
   */
  this.openInit = async () => {
    const res = {
      config: null,
      projects: null,
    };
    await testDir(homeDir);
    res.config = await testDir(`${homeDir}/state.config.json`, true)
      .then(() => readFile(`${homeDir}/state.config.json`))
      .then(data => (data.length ? JSON.parse(data) : null));

    res.projects = await testDir(`${homeDir}/state.projects.json`, true)
      .then(() => readFile(`${homeDir}/state.projects.json`))
      .then(data => (data.length ? JSON.parse(data) : null));
    return res;
  };
  /**
   * Сохранение init настроек
   * @function saveInit
   * @param {Object} data Данные для сохранения
   * @return {Promise} 
   */
  this.saveInit = async data => {
    await testDir(homeDir);
    await writeFile(`${homeDir}/state.config.json`, JSON.stringify(data.config));
    await writeFile(`${homeDir}/state.projects.json`, JSON.stringify(data.projects));
  };
    /**
   * Открытие проекта
   * @function openProject
   * @param {String} uid Уникальные UID проекта
   * @return {Promise<ObjectProject | Error>} Данные проекта из файла
   */
  this.openProject = async uid => {
    await testDir(`${homeDir}/projects/${uid}`);
    await stat(`${homeDir}/projects/${uid}/project.json`);
    const projectData = await readFile(`${homeDir}/projects/${uid}/project.json`);
    return JSON.parse(projectData);
  };
  /**
   * Сохранение проекта
   * @function saveProject
   * @param {String} data Данные проекта
   * @return {Promise} Данные проекта из файла
   */
  this.saveProject = async data => {
    const dir = `${homeDir}/projects/${data.info.uid}/project.json`;
    await testDir(`${homeDir}/projects`);
    await testDir(`${homeDir}/projects/${data.info.uid}`);
    await testDir(dir, true);
    await writeFile(dir, JSON.stringify(data));
  };

  /**
   * Recursive delete project files and folders
   * @function removeProject
   * @param {String} uid Уникальные UID проекта
   * @return {Promise} 
   */
  this.removeProject = uid => {
    const dirStart = `${homeDir}/projects/${uid}`;
    const deleteDir = async dir => {
      await stat(dir);
      const files = await readdir(dir);
      await Promise.all(
        files.map(async file => {
          const p = path.join(dir, file);
          const stats = await stat(p);
          if (stats.isDirectory()) {
            await deleteDir(p);
          } else {
            await unlink(p);
          }
        })
      );
      await rmdir(dir);
    };
    return deleteDir(dirStart);
  };
  /**
   * Функция сохранения нового имени проекта
   * @function saveProjectName
   * @param {ObjectProject} el Объект проекта
   * @return {Promise}
   */
  this.saveProjectName = async (el) => {
    const data = await this.openProject(el.uid);
    data.info.name = el.name;
    await this.saveProject(data);
  }
  /**
   * Функция экспорта проекта на рабочий стол
   * @function exportProject
   * @param {String} uid Уникальные UID проекта
   * @param {String} name Имя проекта
   * @return {Promise}
   */
  this.exportProject = async (uid, name) => {
    const dirProject = `${homeDir}/projects/${uid}/project.json`;
    await stat(dirProject);
    const projectData = await readFile(`${homeDir}/projects/${uid}/project.json`);
    let fileName = `${name}.json`;
    if (fs.existsSync(`${desktopPath}/${fileName}`)) {
      let isExist = true;
      let index = 1;
      while (isExist) {
        fileName = `${name}(${index}).json`;
        isExist = fs.existsSync(`${desktopPath}/${fileName}`);
        index += 1;
      }
    }
    await writeFile(`${desktopPath}/${fileName}`, projectData);
  };
  /**
   * Импорт проекта из файла
   * @function importProject
   * @param {ObjectFile} file File Object для импорта( Browser API) 
   * @return {Promise}
   */
  this.importProject = async file => {
    if (fs.existsSync(file.path)) {
      const projectData = await readFile(file.path);
      return JSON.parse(projectData);
    }
    throw new Error('Файл отсутствует');
  };
  /*
  this.checkUID = async uid => {
    await testDir(`${homeDir}/projects`);
    const files = await readdir(`${homeDir}/projects`);
    return files.find(file => file === uid);
  };
  */
}
