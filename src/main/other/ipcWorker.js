/**
 *
 * IPC worker:
 * TCP-Client ( send net config and configuration)
 * TELNET Echo client
 * Multicast init start
 * fsWorker
 */

import { connector, sender } from '../net/configTCP';
import MulticastBus from '../net/multicastBus';
import FsWorker from './fsWorker';
import FinderDevices from '../net/FinderDevices';
import { ipcMain as ipc } from 'electron'; // eslint-disable-line
import { packData, parserData } from '../service/packagerData';

/*
 *
 * CONSTANTS
 */
const MULTICAST_BUS_IP = '239.1.1.11';
const MULTICAST_BUS_PORT = 3000;
const MULTICAST_FINDER_IP = '239.1.1.11';
const MULTICAST_FINDER_PORT = 3001;
const TCP_CONFIG_PORT = 7676;
/**
 *
 */
const fsWorker = new FsWorker();
let timerCheckMulticastBus = null;
let finderDevices = null;
let multicastBUS = null;
/**
 * Запуск перехвадчика и обработчика событий IPC
 * @function ipcWorkerStart
 * @param {Object} webContents Read Electron.js API
 * @param {Object} eventEmitter
 */
const ipcWorkerStart = (webContents, eventEmitter) => {
  finderDevices = new FinderDevices(eventEmitter, MULTICAST_FINDER_IP, MULTICAST_FINDER_PORT);
  multicastBUS = new MulticastBus(eventEmitter, MULTICAST_BUS_IP, MULTICAST_BUS_PORT);
  // Маленький костыль, чтобы терминал понимал, что телеграммы не ловятся после сна или сворачивания.
  webContents.send('multicastBUS:error');
  /*
   * ConfigTCP device
   *
   */

  /**
   * Function hook status Event config block or net. If Err or Complete - close socket and event about that to Renderer process.
   * @function echoStatusConfig
   * @param {String} status Функция вывода статуса события в консоль
   */
  const echoStatusConfig = status => {
    console.log(status);
    webContents.send('configTCP:response', status);
    if (status.ok) console.log('\x1b[36m%s\x1b[0m', status.text);
    else console.log('\x1b[41m%s\x1b[0m', status.text);
    if (!status.ok || status.text === 'Сonfiguration completed') {
      eventEmitter.removeListener('configTCP:response', echoStatusConfig);
      if (status.socket) status.socket.end();
    }
  };
  /**
   *
   * Configurate device. Blocks
   */
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {Object{ device, addresses}} data
   */
  ipc.on('config:blocks', async (ev, { device, addresses }) => {
    eventEmitter.on('configTCP:response', echoStatusConfig);
    const ip = device.params.actualIP;
    let socket = null;
    try {
      // Connect to device and return socket if success
      socket = await connector(ip, TCP_CONFIG_PORT, eventEmitter);

      if (socket) {
        echoStatusConfig({ ok: true, text: 'Сonfiguration begin' });
        echoStatusConfig({ ok: true, text: 'Start package data' });
        // Пробегаемся по всем объектам всех блоков и приверяем bus
        device.blocks.forEach(block => {
          block.objects.forEach(object => {
            if (object.addresses.length > 0) {
              // Различные проверка для input и output объектов
              /**
               * if need set  busType of address  = 2
               */
              if (object.direction === 'input') {
                let addrLocal = false;
                let addrExt = false;
                let isBusTwo = false;
                object.addresses.forEach(addressID => {
                  const address = addresses.find(el => el.id === addressID);
                  if (address) {
                    if (address.bus === 0) addrLocal = true;
                    if (address.bus === 1) addrExt = true;
                    // Есть ли в адресе еще сокеты с этим устройством.
                    if (
                      address.sockets.filter(socket => socket.device.id === device.id).length > 1 &&
                      address.bus === 1
                    ) {
                      isBusTwo = true;
                    }
                  }
                });
                // Проверка на тип адреса 0 1 2.
                if (isBusTwo || (addrLocal && addrExt)) object.bus = 2;
                else if (addrLocal) object.bus = 0;
                else if (addrExt) object.bus = 1;
              } else if (object.direction === 'output') {
                const address = addresses.find(el => el.id === object.addresses[0]);
                let isBusTwo = false;
                if (address) {
                  if (address.sockets.filter(socket => socket.device.id === device.id).length > 1 && address.bus === 1)
                    isBusTwo = true;
                  if (isBusTwo) object.bus = 2;
                  else object.bus = address.bus;
                }
              }
            }
          });
        });
        // Pack data all blocks of device
        const dataToSend = [];
        device.blocks.forEach((block, index) => {
          dataToSend[index] = packData(block, 'block');
          // Выводим имя функционального блока и его собранный пакет данных
          console.log(block.name);
          console.log(
            dataToSend[index]
              .toString('hex')
              .toUpperCase()
              .match(/.{2}/g)
              .join(':')
          );
        });
        echoStatusConfig({ ok: true, stage: 2, text: 'Data package' });
        // Send data block by block
        /* eslint-disable no-restricted-syntax, no-await-in-loop */
        echoStatusConfig({ ok: true, text: 'Start sending data' });
        for (const blockData of dataToSend) {
          await sender(socket, blockData);
        }
        /* eslint-enable */
        echoStatusConfig({ ok: true, stage: 3, text: 'Data sent!' });

        // sending End datagram
        const dataEnd = packData(device.blocks.length, 'blockEnd');
        const resEnd = await sender(socket, dataEnd);
        if (resEnd) echoStatusConfig({ ok: true, stage: 4, text: 'Сonfiguration completed' });
        else echoStatusConfig({ ok: false, text: 'Сonfiguration error', socket });
      }
    } catch (err) {
      console.log(err);
      echoStatusConfig('configTCP:response', { ok: false, text: err.message || err, socket });
    }
  });
  /**
   *
   * Send Visio config
   */
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {Object{ device}} data
   */
  ipc.on('config:visio', async (ev, { device }) => {
    eventEmitter.on('configTCP:response', echoStatusConfig);
    const ip = device.params.actualIP;
    let socket = null;
    try {
      // Connect to device and return socket if success
      socket = await connector(ip, TCP_CONFIG_PORT, eventEmitter);
      if (socket) {
        echoStatusConfig({ ok: true, text: 'Сonfiguration begin' });
        echoStatusConfig({ ok: true, text: 'Start package data' });
        const dataSend = {};
        dataSend.project = device.project;
        dataSend.statusList = [];
        let menuItems = [];
        // Собираем в один массив все пункты меню
        if (device.project.template === 'Aurora') menuItems = [...device.project.menuItems];
        if (device.project.template === 'Sirius') {
          [...device.project.menuSystems, ...device.project.menuPlaces].forEach(item => {
            if (item.settings.isSubItems) item.subItems.forEach(el => menuItems.push(el));
            else menuItems.push(item);
          });
        }
        // Начинаем собираеться statusList;
        menuItems.forEach(el => {
          // Проверка на структуру виджетов. Если это массив в массиве расшиваем его.
          let widgets = [];
          if (Array.isArray(el.widgets[0])) {
            el.widgets.forEach(el => {
              widgets = widgets.concat(...el);
            });
          } else widgets = el.widgets;
          widgets.forEach(widget => {
            if (widget.addresses) {
              widget.addresses.forEach(el => {
                if (el.status && !dataSend.statusList.find(item => item.status === el.status)) {
                  dataSend.statusList.push({ status: el.status, dataType: el.dataType, value: 0 });
                }
              });
            }
          });
        });
        const bufferSend = Buffer.from(JSON.stringify(dataSend));
        console.log('Длина общего куска');
        console.log(bufferSend.length);
        const bufferArray = [];
        const partLength = 512;
        const countFrame = Math.ceil(bufferSend.length / partLength);
        // creat frameArray
        for (let i = 0; i < countFrame; ) {
          const count = Buffer.alloc(2);
          count.writeUInt16BE(countFrame - i, 0);
          const dataFrame = bufferSend.slice(partLength * i, partLength * (i + 1));
          bufferArray.push(Buffer.concat([count, dataFrame], dataFrame.length + 2));
          i += 1;
        }
        const frameArray = bufferArray.map(el => packData(el, 'visio'));
        echoStatusConfig({ ok: true, stage: 2, text: 'Data package' });
        // Send data frame by frame
        /* eslint-disable no-restricted-syntax, no-await-in-loop */
        console.log('Длина частей');
        bufferArray.forEach(el => {
          console.log(el.length);
          console.log(el.toString());
        });
        echoStatusConfig({ ok: true, text: 'Start sending data' });
        for (const frame of frameArray) {
          await sender(socket, frame);
        }
        echoStatusConfig({ ok: true, stage: 3, text: 'Data sent!' });
        echoStatusConfig({ ok: true, stage: 4, text: 'Сonfiguration completed' });
      }
    } catch (err) {
      console.log(err);
      console.log('Произошла ошибка при отправке');
      echoStatusConfig('configTCP:response', { ok: false, text: err.message || err, socket });
    }
  });
  /**
   *
   * Send settings device
   */
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {Object} device
   */
  ipc.on('config:settings', async (ev, device) => {
    let socket = null;
    try {
      eventEmitter.on('configTCP:response', echoStatusConfig);
      const ip = device.params.actualIP;
      echoStatusConfig({ ok: true, text: 'Сonfiguration begin' });
      socket = await connector(ip, TCP_CONFIG_PORT, eventEmitter);
      if (socket) {
        echoStatusConfig({ ok: true, text: 'Start package data' });
        const data = packData(
          {
            settings: device.settings,
            deviceID: device.id,
            modbus: device.modbus,
            deviceType: device.params.fnTypeCode,
          },
          'settingsDevice'
        );
        echoStatusConfig({ ok: true, stage: 2, text: 'Data package' });
        echoStatusConfig({ ok: true, text: 'Start sending data' });
        const res = await sender(socket, data);
        if (res) echoStatusConfig({ ok: true, stage: 3, text: 'Data sent!' });
        else echoStatusConfig({ ok: false, text: 'Сonfiguration error', socket });
        echoStatusConfig({ ok: true, stage: 4, text: 'Сonfiguration completed' });
        webContents.send('store:UpdateDeviceActual', device);
      }
    } catch (err) {
      console.log(err.message);
      if (socket) socket.end();
      echoStatusConfig('configTCP:response', { ok: false, text: err.message || err, socket });
    }
    //  eventEmitter.emit('configTCP:response', { ok: false, text: 'Problem with connection', socket });
  });
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {Object} device
   */
  ipc.on('config:reqSettings', async (ev, device) => {
    let socket = null;
    try {
      const ip = device.params.actualIP;
      socket = await connector(ip, TCP_CONFIG_PORT, eventEmitter);
      if (socket) {
        const data = packData(null, 'reqSettings');
        const res = await sender(socket, data);
        console.log(res);
        console.log('Ответ на запрос');
        // complete
        if (res && res.readUInt16BE() === 4) {
          const lengthResponse = res.readUInt16BE(2);
          const response = { settings: device.settings, modbus: undefined };
          const settingsValue = parserData(res.slice(6, 46), 'settingsDevice');
          // Прописываем полученные настройки, только те, которые есть в устройстве.
          Object.keys(device.settings).forEach(param => {
            response.settings[param] = settingsValue[param];
          });
          // Прописываем полученные настройки Modbus( Если они есть)
          if (lengthResponse > 42) {
            response.modbus = parserData(res.slice(46, -1), 'settingsModbus');
          }
          // Отправляем результат данных
          ev.sender.send('config:reqSettings:response', { status: 'ok', response });
        } else throw Error('Загрузка не удалась');
        socket.end();
      }
    } catch (err) {
      console.log(err);
      if (socket) socket.end();
      ev.sender.send('config:reqSettings:response', { status: 'error', error: err });
    }
  });
  /**
   *
   *
   *
   * Modbus config
   */
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {Object} device
   */
  ipc.on('config:modbus', async (ev, device) => {
    let socket = null;
    try {
      const ip = device.params.actualIP;
      socket = await connector(ip, TCP_CONFIG_PORT, eventEmitter);
      if (socket) {
        const data = packData({ settings: device.modbus, deviceType: device.params.fnTypeCode }, 'configModbus');
        const res = await sender(socket, data);
        if (res) ev.sender.send('config:modbus:response', { status: 'ok' });
        else throw Error('Загрузка не удалась');
        socket.end();
      }
    } catch (err) {
      console.log(err);
      if (socket) socket.end();
      ev.sender.send('config:modbus:response', { status: 'error', error: err });
    }
  });
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {Object} device
   */
  ipc.on('config:reqModbus', async (ev, device) => {
    let socket = null;
    try {
      const ip = device.params.actualIP;
      socket = await connector(ip, TCP_CONFIG_PORT, eventEmitter);
      if (socket) {
        const data = packData(null, 'reqModbus');
        const res = await sender(socket, data);
        if (res && res.readUInt16BE() === 8) {
          const response = parserData(res.slice(6, -1), 'settingsModbus');
          ev.sender.send('config:reqModbus:response', { status: 'ok', response });
        } else throw Error('Загрузка не удалась');
        socket.end();
      }
    } catch (err) {
      console.log(err);
      if (socket) socket.end();
      ev.sender.send('config:reqModbus:response', { status: 'error', error: err });
    }
  });
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {Object{data, device}} data
   */
  ipc.on('config:sendMsgModbus', async (ev, { data, device }) => {
    let socket = null;
    try {
      const ip = device.params.actualIP;
      socket = await connector(ip, TCP_CONFIG_PORT, eventEmitter);
      if (socket) {
        const dataSend = packData(data, 'sendMsgModbus');
        const res = await sender(socket, dataSend);
        const resData = res.slice(4, res.length - 1);
        const response = {};
        const arrayStatus = ['OK', 'CRC ERROR', 'Connection ERROR', '', 'Slave ERROR'];
        response.channel = resData.readUInt8(0);
        response.address = resData.readUInt8(1);
        response.register = resData.readUInt16BE(2);
        response.type = resData.readUInt8(4);

        response.value = [1, 2, 3, 4].includes(response.type) ? '-' : data.value;
        response.statusValue = resData.readUInt16BE(5);
        response.status = arrayStatus[resData.readUInt8(7)];
        if (res) ev.sender.send('config:sendMsgModbus:response', { status: 'ok', response });
        else throw Error('Загрузка не удалась');
        socket.end();
      }
    } catch (err) {
      console.log(err);
      if (socket) socket.end();
      ev.sender.send('config:sendMsgModbus:response', { status: 'error', error: err });
    }
  });

  /**
   * ONE WIRE
   */
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {Object} device
   */
  ipc.on('config:reqOneWire', async (ev, device) => {
    let socket = null;
    let isResponseSensors = false;
    try {
      const ip = device.params.actualIP;
      socket = await connector(ip, TCP_CONFIG_PORT, eventEmitter);
      if (socket) {
        const data = packData(null, 'reqOneWire');
        const res = await sender(socket, data);
        // complete
        if (res) {
          isResponseSensors = true;
          const numberDevices = Math.floor(res.readUInt16BE(2) / 8);
          const arrayData = res.slice(4, res.length - 1);
          const sensors = [];
          for (let i = 0; i < numberDevices; i++) {
            sensors.push([...arrayData.slice(i * 8, i * 8 + 8)].map(el => el.toString(16)).join(':'));
          }
          ev.sender.send('config:reqOneWire:response', { status: 'ok', sensors });
        } else throw Error('Загрузка не удалась');
        socket.end();
      }
    } catch (err) {
      if (socket) socket.end();
      if (!isResponseSensors) {
        ev.sender.send('config:reqOneWire:response', { status: 'error', error: err });
      }
    }
  });

  ipc.on('config:reqValueOneWire', async (ev, { device, sensors }) => {
    let socket = null;
    try {
      const ip = device.params.actualIP;
      socket = await connector(ip, TCP_CONFIG_PORT, eventEmitter);
      const dataSensors = [];
      sensors.forEach(sensorID => {
        const sensorAddress = sensorID.split(':').map(val => parseInt(val, 16));
        dataSensors.push(...sensorAddress);
      });
      if (socket) {
        const data = packData(dataSensors, 'reqValueOneWire');
        const res = await sender(socket, data);
        // complete
        if (res) {
          const numberValues = Math.floor(res.readUInt16BE(2) / 4);
          const arrayData = res.slice(4, res.length - 1);
          const values = [];
          for (let i = 0; i < numberValues; i++) {
            values.push(arrayData.readFloatBE(i * 4));
          }
          ev.sender.send('config:reqValueOneWire:response', { status: 'ok', values });
        } else throw Error('Загрузка не удалась');
        socket.end();
      }
    } catch (err) {
      console.log(err);
      if (socket) socket.end();
      ev.sender.send('config:reqValueOneWire:response', { status: 'error', error: err });
    }
  });
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {Object{device, table}} data
   */
  ipc.on('config:sendTableKNX', async (ev, { device, table }) => {
    let socket = null;
    try {
      const ip = device.params.actualIP;
      socket = await connector(ip, TCP_CONFIG_PORT, eventEmitter);
      if (socket) {
        const data = packData(table, 'sendTabkeKNX');
        console.log(data);
        const res = await sender(socket, data);
        // complete
        if (res) {
          const status = res.readUInt16BE(0);
          if (status === 20) ev.sender.send('config:sendTableKNX:response', { status: 'ok' });
          else throw new Error('Неверный ответ от устройства');
        } else throw new Error('Загрузка не удалась');
        socket.end();
      }
    } catch (err) {
      console.log(err);
      const message = err.message || 'Проблемы с соединением';
      if (socket) socket.end();
      ev.sender.send('config:sendTableKNX:response', { status: 'error', error: message });
    }
  });

  /**
   * Multicast BUS
   *
   */
  /**
   * Функция прослушки мультикаст шины
   * @function watcherMulticast
   * @param {Object<Socket>} multicast Сокет для прослушивания multicast
   * @param {Object} webContents См. Electron.js API
   */
  const watcherMulticast = (multicast, webContents) => {
    timerCheckMulticastBus = setInterval(() => {
      if (multicast.ready === false && multicast.socket === null) {
        clearInterval(timerCheckMulticastBus);
        multicast.off();
        webContents.send('multicastBUS:error');
      }
    }, 5000);
  };
  /**
   * cb
   */
  ipc.on('multicastBUS:start', () => {
    multicastBUS.start();
    watcherMulticast(multicastBUS, webContents);
    console.log('Команда на запуск шины мультикаст');
  });
  /**
   * cb
   */
  ipc.on('multicastBUS:off', () => {
    multicastBUS.off();
    clearInterval(timerCheckMulticastBus);
    console.log('Команда на остановку шины мультикаст');
    console.log(multicastBUS.ready);
  });

  /**
   * Listen event from Renderer proccess, if user send telegram
   * cb
   * @param {Object} ev Объект события
   * @param {String} msg Сообщение
   */
  ipc.on('multicastBUS:outMessage', (event, msg) => {
    multicastBUS
      .send(msg)
      .then(() => {
        event.sender.send('multicastBUS:outMessageStatus', { status: 'OK', message: msg });
      })
      .catch(err => {
        console.log(err);
        event.sender.send('multicastBUS:outMessageStatus', { status: 'ERROR', error: err });
      });
    // console.log('Исходящие сообщение по multicastBUS');
    // console.log(multicastBUS.ready);
  });
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {String} msg Сообщение
   */
  eventEmitter.on('multicastBUS:message', msg => {
    // console.log('Сообщение multicastBUS', msg);
    webContents.send('multicastBUS:message', msg);
  });
  eventEmitter.on('multicastBUS:ready', () => {
    console.log('MulticastBUS готов');
    webContents.send('multicastBUS:ready');
  });
  eventEmitter.on('multicastBUS:error', () => {
    console.log('MulticastBUS ошибка');
    webContents.send('multicastBUS:error');
  });

  /**
   *  FinderDevices
   *
   */
  /**
   * cb
   */
  ipc.on('finderDevices:start', () => {
    console.log('Команда на запуск поиска устройства');
    finderDevices.start();
  });
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {Object} data Объект устройства
   */
  ipc.on('finderDevices:find', (event, data) => {
    console.log('Команда на поиск устройства');
    const buf = packData(data, 'findDevice');
    finderDevices.send(buf);
  });
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {Object} data Объект устройства
   */
  ipc.on('finderDevices:resetDevice', (event, data) => {
    console.log('Команда на перезагрузку устройства');
    const buf = packData(data, 'resetDevice');
    finderDevices.send(buf);
  });
  /**
   * cb
   */
  ipc.on('finderDevices:off', () => {
    console.log('Команда на выключение сокета поиска устройств');
    finderDevices.off();
  });
  /**
   * cb
   */
  eventEmitter.on('finderDevices:error', () => {
    console.log('Ошибка на сокете. Обнаружено в IPCWORKER');
    webContents.send('finderDevices:error');
  });
  /**
   * cb
   */
  eventEmitter.on('finderDevices:findedDevice', data => {
    // console.log('Одно из устройств ответило');
    // console.log(data);
    webContents.send('finderDevices:findedDevice', data);
  });

  /**
   *
   * Work with Fs request
   *
   */
  /**
   * cb
   * @param {Object} event Объект события
   */
  ipc.on('fs:openInit', event => {
    fsWorker
      .openInit()
      .then(data => {
        event.sender.send('fs:openInitStatus', data);
      })
      .catch(err => {
        event.sender.send('fs:openInitStatus', 'ERROR');
        console.log(err);
      });
  });
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {Object} data Объект настроек для записи
   */
  ipc.on('fs:saveInit', (event, data) => {
    fsWorker
      .saveInit(data)
      .then(() => {
        event.sender.send('fs:saveInitStatus', 'OK');
      })
      .catch(err => {
        event.sender.send('fs:saveInitStatus', 'ERROR');
        console.log(err);
      });
  });
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {String} uid Уникальный UID проекта
   */
  ipc.on('fs:openProject', (event, uid) => {
    fsWorker
      .openProject(uid)
      .then(data => {
        event.sender.send('fs:openProjectStatus', data);
      })
      .catch(err => {
        event.sender.send('fs:openProjectStatus', 'ERROR');
        console.log(err);
      });
  });
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {Object} data Объект проекта
   */
  ipc.on('fs:saveProject', (event, data) => {
    fsWorker
      .saveProject(data)
      .then(() => {
        event.sender.send('fs:saveProjectStatus', 'OK');
      })
      .catch(err => {
        event.sender.send('fs:saveProjectStatus', 'ERROR');
        console.log(err);
      });
  });
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {String} uid Уникальный UID проекта
   */
  ipc.on('fs:removeProject', (event, uid) => {
    fsWorker
      .removeProject(uid)
      .then(() => {
        event.sender.send('fs:removeProjectStatus', 'OK');
      })
      .catch(err => {
        event.sender.send('fs:removeProjectStatus', 'ERROR');
        console.log(err);
      });
  });
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {Object} data Объект проекта
   */
  ipc.on('fs:saveProjectName', (event, data) => {
    fsWorker
      .saveProjectName(data)
      .then(() => {
        event.sender.send('fs:saveProjectName:status', 'OK');
      })
      .catch(err => {
        event.sender.send('fs:saveProjectName:status', 'ERROR');
        console.log(err);
      });
  });
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {Object} data Объект проекта
   */
  ipc.on('fs:exportProject', (event, data) => {
    fsWorker
      .exportProject(data.uid, data.name)
      .then(() => {
        event.sender.send('fs:exportProject:status', 'OK');
      })
      .catch(err => {
        event.sender.send('fs:exportProject:status', 'ERROR');
        console.log(err);
      });
  });
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {Object} file Объект file ( Browser API)
   */
  ipc.on('fs:importProject', (event, file) => {
    fsWorker
      .importProject(file)
      .then(data => {
        event.sender.send('fs:importProject:status', data);
      })
      .catch(err => {
        event.sender.send('fs:importProject:status', null);
        console.log(err);
      });
  });
  /*
  ipc.on('fs:checkUID', (event, uid) => {
    fsWorker
      .checkUID(uid)
      .then(res => {
          console.log(res);
        event.sender.send('fs:removeProjectStatus', res);
      })
      .catch(err => {
        event.sender.send('fs:checkUIDStatus', 'ERROR');
        console.log(err);
      });
  });
  */
  console.log('ipcWorkerStart выполнен');
};

const ipcWorkerEnd = eventEmitter => {
  ipc.removeAllListeners('config:blocks');
  ipc.removeAllListeners('config:settings');
  ipc.removeAllListeners('config:modbus');
  ipc.removeAllListeners('config:reqModbus');
  ipc.removeAllListeners('config:reqOneWire');
  ipc.removeAllListeners('config:sendTableKNX');
  ipc.removeAllListeners('multicastBUS:start');
  ipc.removeAllListeners('multicastBUS:off');
  ipc.removeAllListeners('multicastBUS:outMessage');
  ipc.removeAllListeners('fs:openInit');
  ipc.removeAllListeners('fs:saveInit');
  ipc.removeAllListeners('fs:openProject');
  ipc.removeAllListeners('fs:saveProject');
  ipc.removeAllListeners('fs:removeProject');
  ipc.removeAllListeners('finderDevices:start');
  ipc.removeAllListeners('finderDevices:find');
  ipc.removeAllListeners('finderDevices:resetDevice');
  ipc.removeAllListeners('finderDevices:off');

  eventEmitter.removeAllListeners('configTCP:response');
  eventEmitter.removeAllListeners('multicastBUS:message');
  eventEmitter.removeAllListeners('multicastBUS:ready');
  eventEmitter.removeAllListeners('multicastBUS:error');
  eventEmitter.emit('multicastBUS:off');
  eventEmitter.removeAllListeners('multicastBUS:off');

  eventEmitter.removeAllListeners('finderDevices:findedDevice');
  eventEmitter.removeAllListeners('finderDevices:ready');
  eventEmitter.removeAllListeners('finderDevices:error');
  eventEmitter.emit('finderDevices:off');
  eventEmitter.removeAllListeners('finderDevices:off');

  clearInterval(timerCheckMulticastBus);
  console.log('ipcWorkerEnd выполнен');
  multicastBUS = null;
  finderDevices = null;
};
export { ipcWorkerStart, ipcWorkerEnd };
