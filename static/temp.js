// Модальное окно настроек Функционального блока
el-dialog(title="Настройка функционального блока" :visible.sync="settingBlockVisible")
// Параметры для настройки
| Параметры:
div(v-for="(item, key) in this.configTypes" :key="key")
  div(v-for="(name, index) in item.name" :key="name+index") {{item.nameConfig[name]}}
    el-slider(v-if="item.type === 'range'",
      v-model="settingsConfigBlock[name]",
      :min="item.min", :max="item.max", :step="item.step",
      show-input)
    el-select(v-else-if="item.type === 'select'", size="mini"
      v-model="settingsConfigBlock[name]",
      placeholder="Select")
      el-option(v-for="value in item.value" :value="value" :key="value")
  // Параметры инициализации
| Параметры инициализации:
div(v-for="(item, key) in this.initTypes" :key="key")
  div(v-for="(name, index) in item.name" :key="name+index") {{item.nameConfig[name]}}
    el-slider(v-if="item.type === 'range'",
      v-model="initConfigBlock[name]",
      :min="item.min", :max="item.max", :step="item.step",
      show-input)
    el-select(v-else-if="item.type === 'select'", size="mini"
      v-model="initConfigBlock[name]",
      placeholder="Select")
      el-option(v-for="value in item.value" :value="value" :key="value")

span(slot="footer" class="dialog-footer")
  el-button(@click="settingBlockVisible = false") Отмена
  el-button(type="primary" @click="settingBlockVisible = false") Сохранить

// infoAddress
  <template lang="pug">
div.infoTable
  .btnMenu  Таблица {{this.element.type}}: {{this.element.name}}
  div(v-if="false && this.typeElement === 'element' && !!this.element.objects",
    :class='{activeMouse: this.focus, activeDrop: this.dropFocus}')
    el-table(v-show='!this.dropFocus', :data='this.element.objects', size='mini', empty-text='Нет данных',
      height="400", border)
      el-table-column(prop='moduleID', label='ID Устр-ва', style='padding-bottom: 3px')
      el-table-column(prop='id', label='ID', width='90')
      el-table-column(prop='name', label='Имя объекта')
      el-table-column(label='Открепить')
        template(slot-scope='scope')
          el-button(v-if='scope.row.name', size='mini', type='danger', icon='el-icon-delete',
          @click='clearAddress(scope.row)')
  div(v-else-if="this.typeElement === 'folder'")
    el-table(:data='this.addressArray', size='mini', empty-text='Нет данных', height="400", border)
      el-table-column(prop='id', label='ID Адреса', width='130')
      el-table-column(prop='name', label='Имя адреса')
      el-table-column(prop='count', label='Кол-во объектов')
  .noData(v-else) Нет информации
</template>
// infoObject
<template lang="pug">
div.infoTable
  .btnMenu  Таблица {{this.element.type}}: {{this.element.name}}
  div(v-if=" false && this.typeElement === 'element' && !!this.element.objects")
    el-table(:data='this.element.object', @cell-mouse-enter='select', @cell-mouse-leave='unselect',
      size='mini', empty-text='Нет данных',height="400", border)
      el-table-column(prop='name', label='Название')
      el-table-column(prop='id', label='ID', width='50')
      el-table-column(prop='type', label='Тип')
      el-table-column(prop='address', label='Адрес')
      el-table-column(width='80')
        template(slot-scope='scope')
          div(v-if='!dragAddress')
            el-button(v-if='scope.row.address', size='mini', type='danger', icon='el-icon-delete', @click='clearAddress(scope.row)')
          .activeDrop(v-else='', :class='{activeMouse: scope.row.id === this.rowId }', @dragenter='dragEnter(scope.row)', @dragleave='dragLeave', @dragover.prevent='', @drop.prevent='dropElement')
            | DROP
    el-button(type='success', @click='addObject', plain='')
      | Добавить объект
  div(v-else-if="this.typeElement === 'board' || this.typeElement=== 'room'")
    el-table(:data='this.deviceArray', size='mini', empty-text='Нет данных',height="400", border)
      el-table-column(prop='name', label='Название')
      el-table-column(prop='id', label='ID', width='50')
      el-table-column(prop='type', label='Тип')
      el-table-column(prop='address', label='Адрес')
  .noData(v-else) Нет информации
</template>