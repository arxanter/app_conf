## Структура проекта

```

account-surfer
├── README.md
├── node_modules
├── package.json
├── .gitignore
│
├── .electron-vue /* Файлы конфигурации Webpack для разработки и сборки приложения*/
│   ├── ******
│   └── ******
│
├── build /* Папка расположения собранного приложения*/
│   ├── ******
│   └── ******
│
├── static /* Хранение статитических файлов*/
│   ├── ******
│   └── ******
│
└── src
    ├── main /*Файлы main процесса */
    │   ├──******
    │   └──******
    ├── rendererer /* Файлы renderer процессе */
    │   ├──******
    │   └──******
    └── index.ejs  /*EJS Шаблон приложения для renderer процесса*/

```

## Main процесс

```
│
└── main
    ├── net
    │   ├──configTCP
    │   ├──FinderDevices
    │   ├──multicastBus
    │   └──multicastSocket
    │
    ├── other
    │   ├──fsWorker
    │   └──ipcWorker
    │
    ├── service
    │   ├──getIP
    │   ├──packagerData
    │   └──workCRC
    │
    ├── index.dev.js
    └── index.js
```

#### net/configTCP

###### connector

```
/**
 * Function try connect to device
 * @function connector
 * @param {String} ip
 * @param {Number} port
 * @param {<EventBus>} eventBus
 * @return {Promise} socket
 *
 */
```

###### sender

```
 /**
 * Function send data to device
 * @function sender
 * @param {<SocketObject>} socket Сокет по которому отправлять данные
 * @param {Object} data Данные для отправки
 * @return {Promise} responseData || err
 */
```

#### net/FinderDevices

```
/**
 * Класс поиска устройств.
 * @class FinderDevices
 * @property {<eventEmitter>} eventEmitter Шина для связи с остальным main процессом
 * @property {<eventBUS>} eventBUS Шина для связи с IPC рендер и main процессом
 * @property {String} ip
 * @property {Number} port
 * @property {Socket} socket Сокет общения
 *
 * @method start Запуск периодического опроса устройств
 * @method off  Остановка опроса устройств
 * @method send  Отправка данных по сокету опроса
 * @method errorHandler  Перехватчик ошибок опроса
 * @method msgHandler  Перехватчик сообщений по сокету опроса
 * @method readyHandler  Перехватчик готовности сокета к опросу и обменом сообщениями
 *
 */
```

#### net/multicastBus

###### giveTypeTelegram

```
/**
 * @function giveTypeTelegram
 * @param {Number} value Байт типа телеграммы
 * @return { String} Тип телеграммы
 */
```

###### giveDataTelegram

```
/**
 *  Функция парсинга данных телеграммы
 * @function giveDataTelegram
 * @param {BufferArray} buf Буфер полученных данных
 * @return {Number | String} Команда телеграммы
 */
```

###### parsingTelegram

```
/**
 * Функция парсинга телеграммы
 * @function parsingTelegram
 * @param {ArrayBuffer} buf телеграмма
 * @return {<telegramObject{source, target, type, dataType}>}
 */
```

#### net/multicastSocket

```
/**
 * Класс MulticastSocket. Обертка над socket Node.js
 * @class
 * @property {<eventEmitter>} eventEmitter Шина для связи с остальным main процессом
 * @property {<eventBUS>} eventBUS Шина для связи с IPC рендер и main процессом
 * @property {String} multicastIP
 * @property {Number} multicastPORT
 * @property {Socket} socket Сокет общения
 *
 * @method start Запуск периодического опроса устройств
 * @method off  Остановка опроса устройств
 * @method send  Отправка данных по сокету опроса
 * @method socketStartMulticast  Запуск режима multicast на сокете
 * @method catchErrorEvent  Перехватчик ошибок на сокете
 * @method checkLocalIP  Проверка подключения к сети
 * @method incommingMsg  Перехватчик сообщений по сокету
 *
 */
```

#### other/fsWorker

###### openInit

```
  /**
   * test init files and if miss - creat, or return value
   * @function openInit
   * @return {Object}  initData files
   */
```

###### saveInit

```
  /**
   * Сохранение init настроек
   * @function saveInit
   * @param {Object} data Данные для сохранения
   * @return {Promise}
   */
```

###### openProject

```
  /**
   * Открытие проекта
   * @function openProject
   * @param {String} uid Уникальные UID проекта
   * @return {Promise<ObjectProject | Error>} Данные проекта из файла
   */
```

###### saveProject

```
  /**
   * Сохранение проекта
   * @function saveProject
   * @param {String} data Данные проекта
   * @return {Promise} Данные проекта из файла
   */
```

###### removeProject

```
  /**
   * Recursive delete project files and folders
   * @function removeProject
   * @param {String} uid Уникальные UID проекта
   * @return {Promise}
   */
```

###### saveProjectName

```
  /**
   * Функция сохранения нового имени проекта
   * @function saveProjectName
   * @param {ObjectProject} el Объект проекта
   * @return {Promise}
   */
```

###### exportProject

```
 /**
   * Функция экспорта проекта на рабочий стол
   * @function exportProject
   * @param {String} uid Уникальные UID проекта
   * @param {String} name Имя проекта
   * @return {Promise}
   */
```

###### importProject

```
  /**
   * Импорт проекта из файла
   * @function importProject
   * @param {ObjectFile} file File Object для импорта( Browser API)
   * @return {Promise}
   */
```

#### other/ipcWorker

Основное связующее звено между Main и Renderer процессом

###### ipcWorkerStart

```
  /**
  * Запуск перехвадчика и обработчика событий IPC
  * @function ipcWorkerStart
  * @param {Object} webContents Read Electron.js API
  * @param {Object} eventEmitter
  */
```

###### echoStatusConfig

```
  /**
   * Function hook status Event config block or net. If Err or Complete - close socket and event about that to Renderer process.
   * @function echoStatusConfig
   * @param {String} status Функция вывода статуса события в консоль
   */
```

###### watcherMulticast

```
  /**
   * Функция прослушки мультикаст шины
   * @function watcherMulticast
   * @param {Object<Socket>} multicast Сокет для прослушивания multicast
   * @param {Object} webContents См. Electron.js API
   */
```

###### СОБЫТИЯ

> Конфигурация устройства. Загрузка блоков
> ipc.on('config:blocks', cb)

```
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {Object{ device, addresses}} data
   */
```

> Конфигурация устройства. Загрузка сервера визуализации
> ipc.on('config:visio', cb)

```
  /**
  * cb
  * @param {Object} ev Объект события
  * @param {Object{ device}} data
  */
```

> Конфигурация устройства. Загрузка настроек
> ipc.on('config:settings', cb)

```
  /**
  * cb
  * @param {Object} ev Объект события
  * @param {ObjectDevice} device
  */
```

> Конфигурация устройства. Запрос настроек устройства
> ipc.on('config:reqSettings', cb)

```
  /**
  * cb
  * @param {Object} ev Объект события
  * @param {ObjectDevice} device
  */
```

> Конфигурация устройства. Загрузка настроек modbus
> ipc.on('config:modbus', cb)

```
  /**
  * cb
  * @param {Object} ev Объект события
  * @param {ObjectDevice} device
  */
```

> Конфигурация устройства. Запрос настроек modbus
> ipc.on('config:reqModbus', cb)

```
  /**
  * cb
  * @param {Object} ev Объект события
  * @param {ObjectDevice} device
  */
```

> Отправка modbus сообщения через устройство
> ipc.on('config:sendMsgModbus', cb)

```
  /**
  * cb
  * @param {Object} ev Объект события
  * @param {Object{data, device}} data
  */
```

> Запрос устройств 1-wire
> ipc.on('config:reqOneWire', cb)

```
  /**
  * cb
  * @param {Object} ev Объект события
  * @param {Object} device
  */
```

> Отправка таблицы KNX
> ipc.on('config:sendTableKNX', cb)

```
  /**
  * cb
  * @param {Object} ev Объект события
  * @param {Object{device, table}} data
  */
```

> Запуск прослушивания multicast шины
> ipc.on('multicastBUS:start', cb)

```
  /**
  * cb
  */
```

> Остановка слушателя multicast шины
> ipc.on('multicastBUS:off', cb)

```
  /**
  * cb
  */
```

> Отправка сообщения из renderer процесса
> ipc.on('multicastBUS:outMessage', cb)

```
  /**
  * Listen event from Renderer proccess, if user send telegram
  * cb
  * @param {Object} ev Объект события
  * @param {String} msg Сообщение
  */
```

> Входящее сообщение из шины
> ipc.on('multicastBUS:message', cb)

```
  /**
  * cb
  * @param {Object} ev Объект события
  * @param {String} msg Сообщение
  */
```

> Событие запроса устройств в сети
> ipc.on('finderDevices:start', cb)

```
  /**
  * cb
  */
```

> Входящее сообщение из шины
> ipc.on('finderDevices:find', cb)

```
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {Object} data Объект устройства
   */
```

> Запрос на перезагрузку устройства
> ipc.on('finderDevices:resetDevice', cb)

```
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {Object} data Объект устройства
   */
```

> Запрос на отключение сокета запроса устройств
> ipc.on('finderDevices:off', cb)

```
  /**
  * cb
  */
```

> Ошибка на сокете поиска устройств
> eventEmitter.on('finderDevices:error', cb)

```
  /**
  * cb
  */
```

> Событие ответа устройств по сокету поиска устройств
> eventEmitter.on('finderDevices:findedDevice', cb)

```
  /**
  * cb
  */
```

> Запрос открытия init настроек конфигуратора
> ipc.on('fs:openInit', cb)

```
  /**
   * cb
   * @param {Object} event Объект события
   */
```

> Запрос сохранения init настроек конфигуратора
> ipc.on('fs:saveInit', cb)

```
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {Object} data Объект настроек для записи
   */
```

> Запрос открытия проекта
> ipc.on('fs:openProject', cb)

```
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {String} uid Уникальный UID проекта
   */
```

> Запрос сохранения проекта
> ipc.on('fs:saveProject', cb)

```
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {Object} data Объект проекта
   */
```

> Запрос удаления проекта
> ipc.on('fs:removeProject', cb)

```
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {String} uid Уникальный UID проекта
   */
```

> Запрос сохранения имени проекта
> ipc.on('fs:saveProjectName', cb)

```
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {Object} data Объект проекта
   */
```

> Запрос экспорта проекта
> ipc.on('fs:exportProject', cb)

```
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {Object} data Объект проекта
   */
```

> Запрос импорта проекта
> ipc.on('fs:importProject', cb)

```
  /**
   * cb
   * @param {Object} ev Объект события
   * @param {Object} file Объект file ( Browser API)
   */
```
