import { Terminal } from 'xterm';
import * as fullscreen from 'xterm/lib/addons/fullscreen/fullscreen';
import 'xterm/src/xterm.css';
import 'xterm/lib/addons/fullscreen/fullscreen.css';
import { ipcRenderer } from "electron"; //eslint-disable-line
const telnetDevice = require('electron').remote.getCurrentWindow().objectInfo; //eslint-disable-line
Terminal.applyAddon(fullscreen);
const term = new Terminal({
  cursorBlink: true,
});
term.open(document.getElementById('#terminal'));
term.toggleFullScreen();
term.focus();
const shellprompt = '$ ';
term.prompt = () => {
  term.write(`\r\n${shellprompt}`);
};

// Ожидаем приветственное сообщение
ipcRenderer.on('telnet:helloMsg', (event, data) => {
  term.write(data);
  term.prompt();
})


let dataSend = '';
term.on('key', (key, ev) => {
  const printable =
    !ev.altKey &&
    !ev.altGraphKey &&
    !ev.ctrlKey &&
    !ev.metaKey &&
    ev.keyCode !== 37 &&
    ev.keyCode !== 38 &&
    ev.keyCode !== 39 &&
    ev.keyCode !== 40;
  // Press 'Enter'
  if (ev.keyCode === 13) {

    if (dataSend === 'clear') {
      term.write('\x1b[H\x1b[2J');
      term.write(`${shellprompt}`);
    } else if (dataSend === 'exit') {
      ipcRenderer.send('telnet:close', telnetDevice.id);
    } else {
      const res = ipcRenderer.sendSync('telnet:command', { command: dataSend, deviceID: telnetDevice.id});
      term.write(`\r\n${res}`);
      term.prompt();
    }
    
    dataSend = '';
    // Press 'Delete'
  } else if (ev.keyCode === 8) {
    // Do not delete the prompt
    if (dataSend.length > 0) {
      term.write('\b \b');
      dataSend = dataSend.slice(0, -1);
    }
    // Press another char
  } else if (printable) {
    term.write(key);
    dataSend += key;
  }
});

term.on('paste', data => {
  term.write(data);
  dataSend = data;
});


ipcRenderer.on('telnet:response', (data) => {
  if (data.deviceID === telnetDevice.id) {
    term.write(data.response);
    term.prompt();
  }
})
