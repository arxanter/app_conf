/**
 *
 * Telnet echo terminal
 *
 */
import { BrowserWindow, ipcMain as ipc } from 'electron'; // eslint-disable-line
const net = require('net');
export default function telnetEchoClient() {
  const telnetURL =
    process.env.NODE_ENV === 'development'
      ? 'http://localhost:9080/telnet.html'
      : `file://${__dirname}/telnet.html`;
  /**
   * Initial Telnet Window
   */
  const PORT_TELNET = 23;
  function telnetWindow(objectInfo) {
    const telnetWindow = new BrowserWindow({
      height: 400,
      useContentSize: true,
      width: 800,
      minHeight: 400,
      minWidth: 800,
      fullscreen: false,

    });
    telnetWindow.setMenu(null);
    telnetWindow.loadURL(telnetURL);
    telnetWindow.objectInfo = objectInfo;
    return telnetWindow;
  }

  // Echo Telnet msg between terminal and device
  function echoTelnetWindow(socket, device) {
    ipc.on('telnet:command', (event, data) => {
      if (device.id === data.deviceID) socket.write(data.command);
      const timerError = setTimeout(() => {
        event.returnValue = 'Error send data to device. Problem with connection.';
      }, 3000);
      socket.once('data', data => {
        event.returnValue = data;
        clearTimeout(timerError);
      });
    });
  }

  // !!! Дописать подцепляемся к IP и порту именно этого устройства.
  // Listen Telnet:open and init Window

  const openTelnetDevices = []; // { deviceID, win, socket }

  ipc.on('telnet:open', (event, device) => {
    const telnetClient = openTelnetDevices.find(el => el.deviceID === device.id);
    if (!telnetClient) {
      const socket = new net.Socket();
      socket.setEncoding('utf8');
      let win = null;
      console.log('Try connect telnet');
      socket.connect(PORT_TELNET, device.params.actualIP, () => {
        console.log('Connected. Telnet client.');
        win = telnetWindow(device);
        socket.once('data', data => {
          win.webContents.send('telnet:helloMsg', data);
        });
        echoTelnetWindow(socket, device);
        openTelnetDevices.push({ deviceID: device.id, win, socket });
        win.on('close', () => {
          socket.end('Close connection');
        });
        // Event when socket close
        socket.on('close', () => {
          console.log('Telnet connection closed');
        });
      });
      // Send error response message if can't open socket.
      socket.on('error', (err) => {
        console.log(err);
        if (!win) event.sender.send('telnet:errorOpen', "Can't connect to device");
      });
    } else telnetClient.win.focus();


    // Event close telnet terminal
    ipc.on('telnet:close', (event, deviceID) => {
      const telnetClient = openTelnetDevices.find(el => el.deviceID === deviceID);
      // Небольшой костыль для обработки закрытия окна через команду консоли.
      try {
        telnetClient.win.close();
      } catch (err) {
        console.log(err);
        telnetClient.win.destroy();
        telnetClient.socket.end('Close connection');
      }
    });
  });
}
