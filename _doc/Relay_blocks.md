# Порядок отправки блоков устройства Relay
## - 
## LogicTypeID: 1 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
initStart | arrayUint8 | 8 | 7
mode | arrayUint8 | 8 | 15
offDelay | arrayUint8 | 8 | 23
onDelay | arrayUint8 | 8 | 31
scenes | arrayUint16 | 16 | 39
scenesNumbers | arrayUint8 | 8 | 55
useInScene | arrayUint16 | 16 | 63
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
On/Off ch1 | 31 | 79 
On/Off ch2 | 31 | 110 
On/Off ch3 | 31 | 141 
On/Off ch4 | 31 | 172 
On/Off ch5 | 31 | 203 
On/Off ch6 | 31 | 234 
On/Off ch7 | 31 | 265 
On/Off ch8 | 31 | 296 
Scene | 31 | 327 
Scene save | 31 | 358 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Status ch1 | 7 | 389 
Status ch2 | 7 | 396 
Status ch3 | 7 | 403 
Status ch4 | 7 | 410 
Status ch5 | 7 | 417 
Status ch6 | 7 | 424 
Status ch7 | 7 | 431 
Status ch8 | 7 | 438 
#### Общая длина переданных данных: 445 byte
---- 
 | 7 | 723 
Status ch4 | 7 | 730 
Status ch5 | 7 | 737 
Status ch6 | 7 | 744 
Status ch7 | 7 | 751 
Status ch8 | 7 | 758 
Status ch9 | 7 | 765 
Status ch10 | 7 | 772 
Status ch11 | 7 | 779 
Status ch12 | 7 | 786 
Status ch13 | 7 | 793 
Status ch14 | 7 | 800 
Status ch15 | 7 | 807 
Status ch16 | 7 | 814 
#### Общая длина переданных данных: 821 byte
---- 
