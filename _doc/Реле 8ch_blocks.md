# Порядок отправки блоков устройства Relay
## - 
## LogicTypeID: 1 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
initStart | arrayUint8 | 8 | 7
mode | arrayUint8 | 8 | 15
offDelay | arrayUint8 | 8 | 23
onDelay | arrayUint8 | 8 | 31
scenes | arrayUint16 | 32 | 39
scenesNumbers | arrayUint8 | 16 | 71
useInScene | arrayUint16 | 32 | 87
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
On/Off ch1 | 31 | 119 
On/Off ch2 | 31 | 150 
On/Off ch3 | 31 | 181 
On/Off ch4 | 31 | 212 
On/Off ch5 | 31 | 243 
On/Off ch6 | 31 | 274 
On/Off ch7 | 31 | 305 
On/Off ch8 | 31 | 336 
Scene | 31 | 367 
Scene save | 31 | 398 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Status ch1 | 7 | 429 
Status ch2 | 7 | 436 
Status ch3 | 7 | 443 
Status ch4 | 7 | 450 
Status ch5 | 7 | 457 
Status ch6 | 7 | 464 
Status ch7 | 7 | 471 
Status ch8 | 7 | 478 
#### Общая длина переданных данных: 485 byte
---- 
