# Порядок отправки блоков 
## Thermostat 
## LogicTypeID: 1 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
comfortSetpoint | float | 4 | 7
controlCoolDelay | uint8 | 1 | 11
controlHeatDelay | uint8 | 1 | 12
delatT | float | 4 | 13
hystMode | uint8 | 1 | 17
hysteresis | float | 4 | 18
initStart | uint8 | 1 | 22
kd | float | 4 | 23
ki | float | 4 | 27
kp | float | 4 | 31
mode | uint8 | 1 | 35
nightSetpoint | float | 4 | 36
oneWireAddress | mac | 8 | 40
regulatorType | uint8 | 1 | 48
selectTypeSensor | uint8 | 1 | 49
sendActualTempDelay | uint8 | 1 | 50
standbySetpoint | float | 4 | 51
timeCycle | uint8 | 1 | 55
timePWM | uint8 | 1 | 56
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Input temperature | 31 | 57 
Mode | 31 | 88 
Actual setpoint | 31 | 119 
Block AC | 31 | 150 
Shift setpoint | 31 | 181 
Shift up setpoint bit | 31 | 212 
Shift down setpoint bit | 31 | 243 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Actual temperature | 7 | 274 
Mode status | 7 | 281 
Actual setpoint status | 7 | 288 
Base setpoint status | 7 | 295 
Cool setpoint status | 7 | 302 
Heat setpoint status | 7 | 309 
Heat control(pwm) | 7 | 316 
Heat control(byte) | 7 | 323 
Cool control(pwm) | 7 | 330 
Cool control(byte) | 7 | 337 
Block AC status | 7 | 344 
Heat status | 7 | 351 
Cool status | 7 | 358 
Heat/cool status | 7 | 365 
Error status | 7 | 372 
#### Общая длина переданных данных: 379 byte
---- 
## MotionSensor 
## LogicTypeID: 2 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
blockMode | uint8 | 1 | 7
brightnessHyst | uint8 | 1 | 8
brightnessLevel | uint8 | 1 | 9
dayBlockBehaviour | uint8 | 1 | 10
dayDetectionOffBehaviour | uint8 | 1 | 11
dayDetectionOnBehaviour | uint8 | 1 | 12
dayNightMode | uint8 | 1 | 13
daySendCyclic | uint8 | 1 | 14
dayTimeout | uint8 | 1 | 15
dayValueBlock | uint8 | 1 | 16
dayValueOff | uint8 | 1 | 17
dayValueOn | uint8 | 1 | 18
dependBrightness | uint8 | 1 | 19
minMoution | uint8 | 1 | 20
nightBlockBehaviour | uint8 | 1 | 21
nightDetectionOffBehaviour | uint8 | 1 | 22
nightDetectionOnBehaviour | uint8 | 1 | 23
nightSendCyclic | uint8 | 1 | 24
nightTimeout | uint8 | 1 | 25
nightValueBlock | uint8 | 1 | 26
nightValueOff | uint8 | 1 | 27
nightValueOn | uint8 | 1 | 28
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Moution analog | 31 | 29 
Moution discrete | 31 | 60 
Brightness | 31 | 91 
Day/Night | 31 | 122 
Block detection | 31 | 153 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Day control(1 bit) | 7 | 184 
Day control(1 byte) | 7 | 191 
Night control(1 bit) | 7 | 198 
Night control(1 byte) | 7 | 205 
Block detection status | 7 | 212 
#### Общая длина переданных данных: 219 byte
---- 
## onOffToggle 
## LogicTypeID: 3 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
controlValueReaction | uint8 | 1 | 7
sendReadReqBeforeStart | uint8 | 1 | 8
type | uint8 | 1 | 9
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Control | 31 | 10 
inputStatus | 31 | 41 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Output | 7 | 72 
#### Общая длина переданных данных: 79 byte
---- 
## value 
## LogicTypeID: 4 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
controlValueReaction | uint8 | 1 | 7
sendCyclic | uint8 | 1 | 8
type | uint8 | 1 | 9
value1 | uint8 | 1 | 10
value2 | uint8 | 1 | 11
value3 | float | 4 | 12
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Control | 31 | 16 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Value | 7 | 47 
#### Общая длина переданных данных: 54 byte
---- 
## counter 
## LogicTypeID: 5 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
downValueReaction | uint8 | 1 | 7
maxLimit | uint8 | 1 | 8
minLimit | uint8 | 1 | 9
resetValueReaction | uint8 | 1 | 10
startValue | uint8 | 1 | 11
step | uint8 | 1 | 12
upValueReaction | uint8 | 1 | 13
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Control byte | 31 | 14 
Up | 31 | 45 
Down | 31 | 76 
Reset | 31 | 107 
Status value | 31 | 138 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Output | 7 | 169 
#### Общая длина переданных данных: 176 byte
---- 
## blinds 
## LogicTypeID: 6 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
actionTime | uint16 | 2 | 7
closeControlValue | uint8 | 1 | 9
openControlValue | uint8 | 1 | 10
stopControlValue | uint8 | 1 | 11
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Dimm/Blinds | 31 | 12 
Open | 31 | 43 
Close | 31 | 74 
Stop | 31 | 105 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Control open | 7 | 136 
Control close | 7 | 143 
Status | 7 | 150 
#### Общая длина переданных данных: 157 byte
---- 
## andOr 
## LogicTypeID: 9 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
inputsNumber | uint8 | 1 | 7
sendCyclic | uint8 | 1 | 8
sendReadReqBeforeStart | uint8 | 1 | 9
sendWhenChange | uint8 | 1 | 10
type | uint8 | 1 | 11
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Input 1 | 31 | 12 
Input 2 | 31 | 43 
Input 3 | 31 | 74 
Input 4 | 31 | 105 
Input 5 | 31 | 136 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Output | 7 | 167 
#### Общая длина переданных данных: 174 byte
---- 
## not 
## LogicTypeID: 10 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
sendCyclic | uint8 | 1 | 7
sendReadReqBeforeStart | uint8 | 1 | 8
sendWhenChange | uint8 | 1 | 9
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Input 1 | 31 | 10 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Output | 7 | 41 
#### Общая длина переданных данных: 48 byte
---- 
## compare 
## LogicTypeID: 11 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
dataType | uint8 | 1 | 7
fixValue | uint8 | 1 | 8
fixValueTemp | float | 4 | 9
mode | uint8 | 1 | 13
sendCyclic | uint8 | 1 | 14
sendReadReqBeforeStart | uint8 | 1 | 15
sendWhenChange | uint8 | 1 | 16
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Input 1 | 31 | 17 
Input 2 | 31 | 48 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Output | 7 | 79 
#### Общая длина переданных данных: 86 byte
---- 
## delay 
## LogicTypeID: 12 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
delay | uint16 | 2 | 7
inputType | uint8 | 1 | 9
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Input | 31 | 10 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Output | 7 | 41 
#### Общая длина переданных данных: 48 byte
---- 
## interval 
## LogicTypeID: 13 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
delay | uint16 | 2 | 7
inputType | uint8 | 1 | 9
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Input | 31 | 10 
Block | 31 | 41 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Output | 7 | 72 
#### Общая длина переданных данных: 79 byte
---- 
## calendar 
## LogicTypeID: 14 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
eventType | uint8 | 1 | 7
eventValue | uint8 | 1 | 8
mode | uint8 | 1 | 9
monthDays | arrayBits | 4 | 10
months | arrayBits | 2 | 14
time | uint8 | 2 | 16
weekDays | arrayBits | 1 | 18
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Block | 31 | 19 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Event | 7 | 50 
#### Общая длина переданных данных: 57 byte
---- 
## dimmByValue 
## LogicTypeID: 15 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
delayStep | uint8 | 1 | 7
maxValue | uint8 | 1 | 8
minValue | uint8 | 1 | 9
stepValue | uint8 | 1 | 10
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Dimm | 31 | 11 
Input value(status) | 31 | 42 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Value control | 7 | 73 
Value status | 7 | 80 
#### Общая длина переданных данных: 87 byte
---- 
## mathBlock 
## LogicTypeID: 16 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
expression | string | 50 | 7
inputType | uint8 | 1 | 57
outputType | uint8 | 1 | 58
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Input | 31 | 59 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Output | 7 | 90 
#### Общая длина переданных данных: 97 byte
---- 
## scene 
## LogicTypeID: 17 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
motionSensorScene | uint8 | 1 | 7
standbyScene | uint8 | 1 | 8
timeForMotionSensor | uint8 | 1 | 9
timeForSceneReset | uint8 | 1 | 10
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
MotionSensor | 31 | 11 
Scene invoke | 31 | 42 
Block | 31 | 73 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Scene output | 7 | 104 
Scene status | 7 | 111 
#### Общая длина переданных данных: 118 byte
---- 
## fancoil 
## LogicTypeID: 18 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
mode | uint8 | 1 | 7
speedDepCool | uint8 | 4 | 8
speedDepHeat | uint8 | 4 | 12
speedMode | uint8 | 1 | 16
speedNum | uint8 | 1 | 17
timeCoolFanStart | uint8 | 1 | 18
timeCoolFanStop | uint8 | 1 | 19
timeHeatFanStart | uint8 | 1 | 20
timeHeatFanStop | uint8 | 1 | 21
timeSpeedShift | uint8 | 1 | 22
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Cool input | 31 | 23 
Heat input | 31 | 54 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Speed 1 | 7 | 85 
Speed 2 | 7 | 92 
Speed 3 | 7 | 99 
Speed 4 | 7 | 106 
Speed 5 | 7 | 113 
Heat valve | 7 | 120 
Cool valve | 7 | 127 
Speed status | 7 | 134 
#### Общая длина переданных данных: 141 byte
---- 
## read 
## LogicTypeID: 19 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
delay | uint8 | 1 | 7
inputType | uint8 | 1 | 8
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Read object | 31 | 9 
Block | 31 | 40 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
#### Общая длина переданных данных: 71 byte
---- 
## timeAlarm 
## LogicTypeID: 20 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
eventType | uint8 | 1 | 7
eventValue | uint8 | 1 | 8
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Block | 31 | 9 
Time/day | 31 | 40 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Event | 7 | 71 
Time/day status | 7 | 78 
#### Общая длина переданных данных: 85 byte
---- 
## modbusWrite 
## LogicTypeID: 200 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
channel | uint8 | 1 | 7
errorCycleTime | uint8 | 1 | 8
registerAddress | uint16 | 2 | 9
registerType | uint8 | 1 | 11
sendRequestIfError | uint8 | 1 | 12
slaveAddress | uint8 | 1 | 13
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Control | 31 | 14 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Error | 7 | 45 
#### Общая длина переданных данных: 52 byte
---- 
## modbusRead 
## LogicTypeID: 201 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
channel | uint8 | 1 | 7
errorCycleTime | uint8 | 1 | 8
expression | string | 50 | 9
registerAddress | uint16 | 2 | 59
registerType | uint8 | 1 | 61
sendCyclic | uint8 | 1 | 62
sendRequestIfError | uint8 | 1 | 63
slaveAddress | uint8 | 1 | 64
useExpression | uint8 | 1 | 65
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Status | 7 | 66 
Error | 7 | 73 
#### Общая длина переданных данных: 80 byte
---- 
## multiport 
## LogicTypeID: 202 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
directionDim | uint8 | 1 | 7
initMode | uint8 | 1 | 8
mode | uint8 | 1 | 9
portNumber | uint16 | 2 | 10
portType | uint8 | 1 | 12
sendCyclic | uint8 | 1 | 13
sendWhenChange | uint8 | 1 | 14
timePWM | uint8 | 1 | 15
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Discrete control | 31 | 16 
Heating control | 31 | 47 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Discrete status | 7 | 78 
Analog status | 7 | 85 
Heating control status | 7 | 92 
Long press status | 7 | 99 
Dimm controll | 7 | 106 
#### Общая длина переданных данных: 113 byte
---- 
## oneWire 
## LogicTypeID: 203 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
addressRAM | mac | 8 | 7
deltaT | float | 4 | 15
deviceID | uint8 | 1 | 19
sendCyclic | uint8 | 1 | 20
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Temperature status | 7 | 21 
Error status | 7 | 28 
#### Общая длина переданных данных: 35 byte
---- 
## modbusDimmer 
## LogicTypeID: 204 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
channel | uint8 | 1 | 7
commandCode | uint8 | 1 | 8
errorCycleTime | uint8 | 1 | 9
initStart | uint8 | 1 | 10
maxValue | uint8 | 1 | 11
minValue | uint8 | 1 | 12
registerAddress | uint16 | 2 | 13
scenesNumbers | uint8 | 16 | 15
scenesValue | uint8 | 16 | 31
sendRequestIfError | uint8 | 1 | 47
slaveAddress | uint8 | 1 | 48
useInScene | arrayBits | 2 | 49
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Control | 31 | 51 
SceneInput | 31 | 82 
SceneSave | 31 | 113 
Control on/off | 31 | 144 
Dimm | 31 | 175 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Status dimmer | 7 | 206 
Error | 7 | 213 
Status on/off | 7 | 220 
#### Общая длина переданных данных: 227 byte
---- 
## modbusRGB 
## LogicTypeID: 205 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
channel | uint8 | 1 | 7
commandCode | uint8 | 1 | 8
errorCycleTime | uint8 | 1 | 9
initStart | uint8 | 1 | 10
maxValue | uint8 | 1 | 11
minValue | uint8 | 1 | 12
registerAddressChA | uint16 | 2 | 13
registerAddressChB | uint16 | 2 | 15
registerAddressChC | uint16 | 2 | 17
registerAddressChD | uint16 | 2 | 19
scenesNumbers | uint8 | 16 | 21
scenesValue | arrayUint8 | 64 | 37
sendRequestIfError | uint8 | 1 | 101
slaveAddress | uint8 | 1 | 102
useInScene | arrayBits | 2 | 103
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Hue | 31 | 105 
Control ch A | 31 | 136 
Control ch B | 31 | 167 
Control ch C | 31 | 198 
Control ch D | 31 | 229 
SceneInput | 31 | 260 
SceneSave | 31 | 291 
Color | 31 | 322 
Hue dimm | 31 | 353 
On/Off | 31 | 384 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Status ch A | 7 | 415 
Status ch B | 7 | 422 
Status ch C | 7 | 429 
Status ch D | 7 | 436 
Error | 7 | 443 
Status color | 7 | 450 
Status on/off | 7 | 457 
#### Общая длина переданных данных: 464 byte
---- 
## modbusRGBLightTemp 
## LogicTypeID: 206 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
channel | uint8 | 1 | 7
commandCode | uint8 | 1 | 8
errorCycleTime | uint8 | 1 | 9
initStart | uint8 | 1 | 10
maxValue | uint8 | 1 | 11
minValue | uint8 | 1 | 12
registerAddressChA | uint16 | 2 | 13
registerAddressChB | uint16 | 2 | 15
registerAddressChC | uint16 | 2 | 17
registerAddressChD | uint16 | 2 | 19
scenesNumbersAB | uint8 | 16 | 21
scenesNumbersCD | uint8 | 16 | 37
scenesValueAB | arrayUint8 | 32 | 53
scenesValueCD | arrayUint8 | 32 | 85
sendRequestIfError | uint8 | 1 | 117
slaveAddress | uint8 | 1 | 118
useInSceneAB | arrayBits | 2 | 119
useInSceneCD | arrayBits | 2 | 121
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
ColorTempAB | 31 | 123 
ColorTempCD | 31 | 154 
Control ch A | 31 | 185 
Control ch B | 31 | 216 
Control ch C | 31 | 247 
Control ch D | 31 | 278 
SceneInputAB | 31 | 309 
SceneInputCD | 31 | 340 
SceneSaveAB | 31 | 371 
SceneSaveCD | 31 | 402 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Status ch A | 7 | 433 
Status ch B | 7 | 440 
Status ch C | 7 | 447 
Status ch D | 7 | 454 
Error | 7 | 461 
#### Общая длина переданных данных: 468 byte
---- 
## modbusRelay 
## LogicTypeID: 207 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
blinkDelay | uint8 | 1 | 7
channel | uint8 | 1 | 8
commandCode | uint8 | 1 | 9
errorCycleTime | uint8 | 1 | 10
initStart | uint8 | 1 | 11
mode | uint8 | 1 | 12
offDelay | uint8 | 1 | 13
onDelay | uint8 | 1 | 14
registerAddress | uint16 | 2 | 15
scenesNumbers | uint8 | 16 | 17
scenesValue | arrayBits | 2 | 33
sendRequestIfError | uint8 | 1 | 35
slaveAddress | uint8 | 1 | 36
useInScene | arrayBits | 2 | 37
valueOff | uint8 | 1 | 39
valueOn | uint8 | 1 | 40
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Control | 31 | 41 
SceneInput | 31 | 72 
SceneSave | 31 | 103 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Status relay | 7 | 134 
Error | 7 | 141 
#### Общая длина переданных данных: 148 byte
---- 
