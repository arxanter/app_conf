####Проект Aurora

```javascript
  {
  name: String,/* Имя шаблона */
  template: String, /* Тип шаблона проекта. = 'Aurora' */
  menuItems: [/*menuItems*/],
  }
```

####Проект Sirius

```javascript
{
  name: String, /* Имя шаблона */
  template: String, /* Тип шаблона проекта. = 'Sirius' */
  menuSystems: [/*menuItems*/], /* Элементы меню раздел "Левая кнопка" */
  menuPlaces: [/*menuItems*/],/* Элементы меню раздел "Правая кнопка" */
}
```

#### Элемент меню

```javascript
{
  settings: {
    name: String,  /* Имя элемента меню */
    templateType: String,  /* Шаблон отображения виджетов данного элемента меню. */
    /* Возможен вариант: 'ColumnsTemplate' - 3 стобца, 'SingleWidget' - 1 виджет */
    icon: String, /* Иконка элемента меню */
    isSubItems: Boolean, /* Флаг, определяющий имеет ли меню вложенные пункты */
    isChildItem: Boolean, /* Флаг, сообщающий, что это дочерний пункт меню */
  },
  type: String, /* Тип элемента. Необходим для конфигуратора */
  /* Массив виджетов данного пункта меню */
    /* Для templateType: 'ColumnsTemplate' */
    widgets: [[/*widgets*/], [/*widgets*/], [/*widgets*/]],
    /* Для  templateType: 'SingleWidget' */
    widgets: [/*widgets*/]
  /**/
  subItems: [/*menuItems*/], /* Массив дочерних элементов меню */
  uid: String, /* Уникальный для проекта ID элемента меню */
}

```

#### Виджеты

**Общая структура**

_Widget_

```javascript
{
    widgetType: String, /* Тип виджета */
    type: 'widget', /* Тип элемента(для конфигуратора) */
    settings: {}, /* Настройки виджета */
    addresses: [/*addresses*/]  /* Список адресов виджета */
}
```

_Address_

```javascript
{
  valueName: String; /* Имя параметра для объекта адреса */
  address: Number; /* Адрес, если происходит запись по данном параметру( опционально)*/
  status: Number; /* Адрес, если происходит чтение по данном параметру( опционально)*/
  dataType: Number; /* Тип данных данного объекта адреса */
}
```

**Структура виджетов**

###### Climate

```javascript
{
  widgetType: 'wClimate',
  type: 'widget',
  settings: {
    name: String, /* Имя данного виджета */
    step: Number /* Шаг регулирования температуры */
  },
  addresses: [
    { valueName: 'setpoint', address: Number, status: Number, dataType: 6 },
    { valueName: 'mode', address: Number, status: Number, dataType: 2 },
    { valueName: 'value', status: Number, dataType: 6 },
    { valueName: 'heat', status: Number, dataType: 1 },
    { valueName: 'cool', status: Number, dataType: 1 },
  ],
}
```

###### Header

```javascript
{
  widgetType: 'wHeader',
  type: 'widget',
  settings: {
    name: String, /* Имя данного виджета */
  },
  addresses: []
},
```

###### InputNumber

```javascript
{
  widgetType: 'wInputNumber',
  type: 'widget',
  settings: {
    name: String, /* Имя данного виджета */
    step: Number,
    min: Number,
    max: Number,
  },
  addresses: [{ valueName: 'value', address: Number, status: Number, dataType: 2 }],
}
```

###### LED

```javascript
{
  widgetType: 'wLED',
  type: 'widget',
  settings: {
    name: 'Значение',
  },
  addresses: [
    { valueName: 'value', address: Number, status: Number, dataType: 7 },
    { valueName: 'toggle', address: Number, status: Number, dataType: 1 },
  ],
}
```


###### Scenes

```javascript
{
  widgetType: 'wScenes',
  type: 'widget',
  settings: {
    name: String, /* Имя данного виджета */
    countScenes: Number, /* Кол-во сцен 1- 8*/
    /* Массив сцен */
    scenes: [
      {
        icon: ['far', 'lightbulb'], /* Описание иконки для библиотеки fontawesome */
        value: Number,
      },
      {
        icon: ['far', 'lightbulb'],
        value: Number,
      },
      {
        icon: ['far', 'lightbulb'],
        value: Number,
      },
      {
        icon: ['far', 'lightbulb'],
        value: Number,
      },
      {
        icon: ['far', 'lightbulb'],
        value: Number,
      },
      {
        icon: ['far', 'lightbulb'],
        value: Number,
      },
      {
        icon: ['far', 'lightbulb'],
        value: Number,
      },
      {
        icon: ['far', 'lightbulb'],
        value: Number,
      },
    ],
  },
  addresses: [{ valueName: 'value', address: Number, dataType: 2 }],
}
```

###### Slider

```javascript
{
  widgetType: 'wSlider',
  type: 'widget',
  settings: {
    name: String, /* Имя данного виджета */
    demension: String, /* Еденица измерения ('%', '') */
    isSwitch: Boolean, /* Флаг отображения переключателя для слайдера*/
    min: Number,
    max: Number,
  },
  addresses: [
    { valueName: 'value', address: Number, status: Number, dataType: 2 },
    { valueName: 'toggle', address: Number, status: Number, dataType: 1 },
  ],
}
```


###### StatusIcon

```javascript
{
  widgetType: 'wStatusIcon',
  type: 'widget',
  settings: {
    name: String, /
    countState:  Number, /* Кол-во состояний. Max = 5*/
    /* Список состояний */
    states: [
      {
        icon: ['far', 'lightbulb'], /* Описание иконки для библиотеки fontawesome */
        color: String, /* Цвет иконки*/
        value: Number, /* Значение состояния */
      },
      {
        icon: ['far', 'lightbulb'], /* Описание иконки для библиотеки fontawesome */
        color: String, /* Цвет иконки*/
        value: Number, /* Значение состояния */
      },
      {
        icon: ['far', 'lightbulb'], /* Описание иконки для библиотеки fontawesome */
        color: String, /* Цвет иконки*/
        value: Number, /* Значение состояния */
      },
      {
        icon: ['far', 'lightbulb'], /* Описание иконки для библиотеки fontawesome */
        color: String, /* Цвет иконки*/
        value: Number, /* Значение состояния */
      },
      {
        icon: ['far', 'lightbulb'], /* Описание иконки для библиотеки fontawesome */
        color: String, /* Цвет иконки*/
        value: Number, /* Значение состояния */
      },
    ],
  },
  addresses: [{ valueName: 'value', status: Number, dataType: 1 }],
}
```


###### StatusValue

```javascript
{
  widgetType: 'wStatusValue',
  type: 'widget',
  settings: {
    name: String, /* Имя данного виджета */
    demension: String, /* Ед. измерения(°C, Бар, Па) */
  },
  addresses: [{ valueName: 'value', status: Number, dataType: 2 }],
}
```

###### StatusValue

```javascript
{
  widgetType: 'wSwitch',
  type: 'widget',
  settings: {
    name: String, /* Имя данного виджета */
    inverse: Boolean, /* Флаг инверсии */
    valueOn: Number, /* Значение для Off */
    valueOff: Number, /* Значение для On */
  },
  addresses: [{ valueName: 'value', address: Number, status: Number, dataType: 1 }],
}
```