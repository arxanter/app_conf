# Порядок отправки блоков устройства Relay
## - 
## LogicTypeID: 1 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
initStart | arrayUint8 | 16 | 7
mode | arrayUint8 | 16 | 23
offDelay | arrayUint8 | 16 | 39
onDelay | arrayUint8 | 16 | 55
scenes | arrayUint16 | 32 | 71
scenesNumbers | arrayUint8 | 16 | 103
useInScene | arrayUint16 | 32 | 119
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
On/Off ch1 | 31 | 151 
On/Off ch2 | 31 | 182 
On/Off ch3 | 31 | 213 
On/Off ch4 | 31 | 244 
On/Off ch5 | 31 | 275 
On/Off ch6 | 31 | 306 
On/Off ch7 | 31 | 337 
On/Off ch8 | 31 | 368 
On/Off ch9 | 31 | 399 
On/Off ch10 | 31 | 430 
On/Off ch11 | 31 | 461 
On/Off ch12 | 31 | 492 
On/Off ch13 | 31 | 523 
On/Off ch14 | 31 | 554 
On/Off ch15 | 31 | 585 
On/Off ch16 | 31 | 616 
Scene | 31 | 647 
Scene save | 31 | 678 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Status ch1 | 7 | 709 
Status ch2 | 7 | 716 
Status ch3 | 7 | 723 
Status ch4 | 7 | 730 
Status ch5 | 7 | 737 
Status ch6 | 7 | 744 
Status ch7 | 7 | 751 
Status ch8 | 7 | 758 
Status ch9 | 7 | 765 
Status ch10 | 7 | 772 
Status ch11 | 7 | 779 
Status ch12 | 7 | 786 
Status ch13 | 7 | 793 
Status ch14 | 7 | 800 
Status ch15 | 7 | 807 
Status ch16 | 7 | 814 
#### Общая длина переданных данных: 821 byte
---- 
