# Порядок отправки блоков устройства Relay
## - 
## LogicTypeID: 1 
#### Параметры блока:
Название | Размер | Позиция в масиве
--- | --- | --- 
 ID блока | 1 | 0
 Тип блока | 1 | 1
 HASH_UID | 4 | 2
 clearVariables | 1 | 6
#### Настройки:
Имя параметра | Тип значения | Размер | Позиция в масиве
--- | --- | --- | ---
initStart | arrayUint8 | 4 | 7
mode | arrayUint8 | 4 | 11
offDelay | arrayUint8 | 4 | 15
onDelay | arrayUint8 | 4 | 19
scenes | arrayUint16 | 32 | 23
scenesNumbers | arrayUint8 | 16 | 55
useInScene | arrayUint16 | 32 | 71
#### Объекты INPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
On/Off ch1 | 31 | 103 
On/Off ch2 | 31 | 134 
On/Off ch3 | 31 | 165 
On/Off ch4 | 31 | 196 
Scene | 31 | 227 
Scene save | 31 | 258 
#### Объекты OUTPUT:
Имя | Размер | Позиция в масиве
--- | --- | --- 
Status ch1 | 7 | 289 
Status ch2 | 7 | 296 
Status ch3 | 7 | 303 
Status ch4 | 7 | 310 
#### Общая длина переданных данных: 317 byte
---- 
